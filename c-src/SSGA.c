#include <stdio.h>
#include <math.h> /* 数学関数を使�?ためのヘッダファイルのインクルー�? */
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#define N 32
#define NBIT 256
#define MAX_GEN 1024
#define WEIGHT 1000

double mrate=0.03; /* 関数 mutation で使�?ための mrate の定義 */

double chrom_info[NBIT][2];
double func_c,roulette_c,cross_c,mutation_c,minimum_c;
double glo_sum,glo_min;

void main()
{
  double start,end;
  int chrom[N][NBIT];
  double fit[N];
  int i,j,index[2]={0,0};
  int gen;
  FILE *fp;
  char *filename="napsac.csv";
  void init(int [][NBIT]); /* 関数 init の型、引数の型等�?�宣言 */
  void print0(int [][NBIT]); /* 表示用関数の宣言 */
  void func(int [][NBIT],double *); /* 関数 func の型、引数の 型等�?�宣言 */
  void print1(int [][NBIT], double [N]); /* 表示用関数の宣 言 */
  void minimum(double *val,int *i);
  int roulette(double *); /* 関数 roulette の型、引数の型等�?� 宣言 */
  void cross(int [][NBIT],int ,int, int*); /* 関数 cross の型、引数の型等�?�宣言 */
  void mutation(int [][NBIT]); /* 関数 mutation の型、引数の型等�?�宣言 */
  double find_max(double *);
  double second(void);
 
  func_c=0;
  roulette_c=0;
  cross_c=0;
  mutation_c=0;
  minimum_c=0;

  printf("Chromosome : %d\nElement : %d\nGeneration : %d\n\n",N,NBIT,MAX_GEN);
  
  start = second();
  
  if((fp=fopen(filename,"r")) == NULL) {
    fprintf(stderr, "Fail open error.\n");
    exit(EXIT_FAILURE);
  }
  i=0;
  int t;
  while(fscanf(fp,"%d \t%lf \t%lf \n",&t,&chrom_info[i][0],&chrom_info[i][1])!=EOF) i++;
  close(fp);
  init(chrom); /* 染色体�?�初期�? */

  //print0(chrom); /* chrom を表示 */
  for(gen=0;gen<MAX_GEN;gen++){
    func(chrom,fit); /* 関数値を計算し、zに代入して戻�? */
    if(gen==0 || gen%10==1) printf("GEN:%d\taverage:%f\tmax:%f\tmin:%f\n",gen,glo_sum/N,find_max(fit),glo_min);
    if(gen==MAX_GEN-1) break;
    minimum(fit,index);
    cross(chrom,roulette(fit),roulette(fit),index); /* 交�? */
    mutation(chrom); /* 突然変異 */
  }
  end = second();/*
  printf("PROGRAM RUN TIME : %f\n",end-start);
  printf("FUNC RUN TIME : %f\n",func_c);
  printf("ROULETTE RUN TIME : %f\n",roulette_c);
  printf("MINIMUM RUN TIME : %f\n",minimum_c);
  printf("CROSS RUN TIME : %f\n",cross_c);
  printf("MUTATION RUN TIME : %f\n",mutation_c);*/
}

void init(int x[][NBIT])
{
  int i,j;
  float rnd(short int); /* float型�?�関数rndを呼ぶための準備 */
  rnd(9/*(unsigned)time(NULL)*2+1*/);
  for(i=0;i<N;i++)
    {
      for(j=0;j<NBIT;j++)
	{
	  if(rnd(1)<=0.5)
	    *(*x+i*NBIT+j)=0; /* x[i][j]=0; と同じ */
	  else
	    *(*x+i*NBIT+j)=1; /* x[i][j]=1; と同じ */
	}
    }
  return;
}


void print0(int chrom[][NBIT])
{
  int i,j;
  for(i=0;i<N;i++)
    {
      printf("%3d:",i);
      for(j=0;j<NBIT;j++) printf("%1d",chrom[i][j]);
      printf("\n");
    }
  printf("\n");
  return;
}


float rnd(short int x)
{
  static short int ix=1,init_on=0;

  if(((x%2)!=0) && (init_on==0))
    {ix=x; init_on=1;}
  ix=899*ix; if(ix<0) ix=ix+32767+1;
  return((float)ix/32768.0);
}


void func(int chrom[][NBIT],double *x)
{  
  double second(void );
  double start,end;
  int i,j;
  double chrom_weight;
  start=second();
  for(i=0;i<N;i++)
    {
      x[i]=0;
      chrom_weight=0;
      for(j=0;j<NBIT;j++)
	{
	  if(chrom[i][j]==1){
	    x[i]+=chrom_info[j][1];
	    chrom_weight+=chrom_info[j][0];
	  }
	  if(chrom_weight>WEIGHT){
	    x[i]=0;
	    break;
	    }
	}
	    
    }
  end=second();
  func_c+=end-start;
  return;
}

void print1(int chrom[][NBIT],double x[N])
{
  int i,j;
  for(i=0;i<N;i++)
    {
      printf("%3d: GEN : ",i);
      for(j=0;j<NBIT;j++) printf("%1d",chrom[i][j]);
      printf("\n\tVALUE : %f",x[i]);
      printf("\n");
    }
  printf("\n");
  return;
}

void minimum(double *val,int *i)
{  
  double second(void );
  double start=second(),end;
  int j;
  double min1=val[0],min2=val[1],tmp;
  i[0]=0;
  i[1]=1;
  for(j=2;j<N;j++){
    if((tmp=val[j])<min1 && tmp>min2){
      min1=tmp;
      i[0]=j;
    }else if(tmp<min2 && tmp>min1){
      min2=tmp;
      i[1]=j;
    }else if(tmp<=min1 && tmp<=min2){
      if(min1<min2){
	min2=tmp;
	i[1]=j;
      }else{
	min1=tmp;
	i[0]=j;
      }
    }
  }
  if(min1<min2) glo_min=min1;
  else glo_min=min2;
  end=second();
  minimum_c+=end-start;
  return;
}

int roulette(double *val)
{  
  double second(void );
  double start,end;
  int i;
  double prob=0,sum=0;
  float rnd(short int), rnd0;
  start=second();
  rnd0=rnd(1);
  for(i=0;i<N;i++)
    {
      sum+=*(val+i);
    }
  for(i=0;i<N;i++)
    {
      prob+=*(val+i)/sum;
      if((double)rnd0<prob) break; 
    }
  glo_sum=sum;
  end=second();
  roulette_c+=end-start;
  return i;
}


void cross(int chrom[][NBIT],int i1,int i2,int *rm)
{ 
  double second(void );
  double start=second(),end;
  int i,j,*address,cross_point,*chng=(int*)malloc(sizeof(int)*NBIT*2);
  float rnd(short int);

  cross_point=(int)(rnd(1)*(float)(NBIT-1))+1;
  for(j=0;j<cross_point;j++)
    {
      chng[j]=*(*chrom+NBIT*i1+j);
      chng[NBIT+j]=*(*chrom+NBIT*i2+j);
    }
  for(j=cross_point;j<NBIT;j++)
    {
      chng[NBIT+j]=*(*chrom+NBIT*i1+j);
      chng[j]=*(*chrom+NBIT*i2+j);
    }
    for(j=0;j<NBIT;j++)
      {
      address=*chrom+NBIT*rm[0]+j;
      *address=chng[j];
      }
    for(j=0;j<NBIT;j++)
      {
      address=*chrom+NBIT*rm[1]+j;
      *address=chng[NBIT+j];
      }
  free(chng);

  end=second();
  cross_c+=end-start;
  return;
}

void mutation(int x[][NBIT])
{
  double second(void );
  double start=second(),end;
  int i,j,mutation_point,*adr_x;
  float rnd(short int);

  for(i=0;i<N;i++)
    {
      if(rnd(1)<=mrate)
	{
	  mutation_point=(int)(rnd(1)*(float)NBIT);
	  adr_x=*x+NBIT*i+mutation_point;
	      if(*adr_x==1)
		*adr_x=0;
	      else
		*adr_x=1;
	}
    }
  end=second();
  mutation_c+=end-start;
  return;
}




double find_max(double *val)
{
  int i;
  double max=-10;
  for(i=0;i<N;i++)
    {
      if(*(val+i)>max) max=*(val+i);
    }
  return max;
}

double second(){

  struct timeval tm;
  double t;

  static int base_sec=0, base_usec=0;

  gettimeofday(&tm, NULL);

  if(base_sec == 0 && base_usec == 0){
    base_sec  = tm.tv_sec;
    base_usec = tm.tv_usec;
    t = 0.0;
  } else {
    t = (double)(tm.tv_sec-base_sec) + ((double)(tm.tv_usec-base_usec))/1.0e6;
  }

  return t;
}
