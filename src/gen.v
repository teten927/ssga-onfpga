/* declare tournament1 */

module gen(p_reset, m_clock, write, tournament, tournament2, select, r0out, num, in_val, out_fig0, out_fig1, out_fig, out_val);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input write;
  wire write;
  input tournament;
  wire tournament;
  input tournament2;
  wire tournament2;
  input select;
  wire select;
  input [19:0] r0out;
  wire [19:0] r0out;
  input [4:0] num;
  wire [4:0] num;
  input [16:0] in_val;
  wire [16:0] in_val;
  output [4:0] out_fig0;
  wire [4:0] out_fig0;
  output [4:0] out_fig1;
  wire [4:0] out_fig1;
  output [4:0] out_fig;
  wire [4:0] out_fig;
  output [16:0] out_val;
  wire [16:0] out_val;
  /* elements */
  reg [4:0] sf0;
  reg [4:0] sf1;
  reg [4:0] sf2;
  reg [4:0] sf3;
  reg [16:0] sv0;
  reg [16:0] sv1;
  reg [16:0] sv2;
  reg [16:0] sv3;
  reg [4:0] fig0;
  reg [4:0] fig1;
  reg [4:0] fig2;
  reg [4:0] fig3;
  reg [4:0] fig4;
  reg [4:0] fig5;
  reg [4:0] fig6;
  reg [4:0] fig7;
  reg [4:0] fig8;
  reg [4:0] fig9;
  reg [4:0] fig10;
  reg [4:0] fig11;
  reg [4:0] fig12;
  reg [4:0] fig13;
  reg [4:0] fig14;
  reg [4:0] fig15;
  reg [4:0] fig16;
  reg [4:0] fig17;
  reg [4:0] fig18;
  reg [4:0] fig19;
  reg [4:0] fig20;
  reg [4:0] fig21;
  reg [4:0] fig22;
  reg [4:0] fig23;
  reg [4:0] fig24;
  reg [4:0] fig25;
  reg [4:0] fig26;
  reg [4:0] fig27;
  reg [4:0] fig28;
  reg [4:0] fig29;
  reg [4:0] fig30;
  reg [4:0] fig31;
  reg [16:0] val0;
  reg [16:0] val1;
  reg [16:0] val2;
  reg [16:0] val3;
  reg [16:0] val4;
  reg [16:0] val5;
  reg [16:0] val6;
  reg [16:0] val7;
  reg [16:0] val8;
  reg [16:0] val9;
  reg [16:0] val10;
  reg [16:0] val11;
  reg [16:0] val12;
  reg [16:0] val13;
  reg [16:0] val14;
  reg [16:0] val15;
  reg [16:0] val16;
  reg [16:0] val17;
  reg [16:0] val18;
  reg [16:0] val19;
  reg [16:0] val20;
  reg [16:0] val21;
  reg [16:0] val22;
  reg [16:0] val23;
  reg [16:0] val24;
  reg [16:0] val25;
  reg [16:0] val26;
  reg [16:0] val27;
  reg [16:0] val28;
  reg [16:0] val29;
  reg [16:0] val30;
  reg [16:0] val31;
  wire sort0;
  wire sort1;
  wire sort2;
  wire sort3;
  /* reg decls */
  reg __stage_COMP;
  reg __task_COMP_comp;
  /* net decls */
  wire __net1477;
  wire __stage_COMP_reset;
  wire __stage_COMP_set;
  wire __task_COMP_comp_set;
  wire __net1476;
  wire __net1475;
  wire __net1474;
  wire __net1473;
  wire __net1472;
  wire __net1471;
  wire __net1470;
  wire __net1469;
  wire __net1468;
  wire __net1467;
  wire __net1466;
  wire __net1465;
  wire __net1464;
  wire __net1463;
  wire __net1462;
  wire __net1461;
  wire __net1460;
  wire __net1459;
  wire __net1458;
  wire __net1457;
  wire __net1456;
  wire __net1455;
  wire __net1454;
  wire __net1453;
  wire __net1452;
  wire __net1451;
  wire __net1450;
  wire __net1449;
  wire __net1448;
  wire __net1447;
  wire __net1446;
  wire __net1445;
  wire __net1444;
  wire __net1443;
  wire __net1442;
  wire __net1441;
  wire __net1440;
  wire __net1439;
  wire __net1438;
  wire __net1437;
  wire __net1436;
  wire __net1435;
  wire __net1434;
  wire __net1433;
  wire __net1432;
  wire __net1431;
  wire __net1430;
  wire __net1429;
  wire __net1428;
  wire __net1427;
  wire __net1426;
  wire __net1425;
  wire __net1424;
  wire __net1423;
  wire __net1422;
  wire __net1421;
  wire __net1420;
  wire __net1419;
  wire __net1418;
  wire __net1417;
  wire __net1416;
  wire __net1415;
  wire __net1414;
  wire __net1413;
  wire __net1412;
  wire __net1411;
  wire __net1410;
  wire __net1409;
  wire __net1408;
  wire __net1407;
  wire __net1406;
  wire __net1405;
  wire __net1404;
  wire __net1403;
  wire __net1402;
  wire __net1401;
  wire __net1400;
  wire __net1399;
  wire __net1398;
  wire __net1397;
  wire __net1396;
  wire __net1395;
  wire __net1394;
  wire __net1393;
  wire __net1392;
  wire __net1391;
  wire __net1390;
  wire __net1389;
  wire __net1388;
  wire __net1387;
  wire __net1386;
  wire __net1385;
  wire __net1384;
  wire __net1383;
  wire __net1382;
  wire __net1381;
  wire __net1380;
  wire __net1379;
  wire __net1378;
  wire __net1377;
  wire __net1376;
  wire __net1375;
  wire __net1374;
  wire __net1373;
  wire __net1372;
  wire __net1371;
  wire __net1370;
  wire __net1369;
  wire __net1368;
  wire __net1367;
  wire __net1366;
  wire __net1365;
  wire __net1364;
  wire __net1363;
  wire __net1362;
  wire __net1361;
  wire __net1360;
  wire __net1359;
  wire __net1358;
  wire __net1357;
  wire __net1356;
  wire __net1355;
  wire __net1354;
  wire __net1353;
  wire __net1352;
  wire __net1351;
  wire __net1350;
  wire __net1349;
  wire __net1348;
  wire __net1347;
  wire __net1346;
  wire __net1345;
  wire __net1344;
  wire __net1343;
  wire __net1342;
  wire __net1341;
  wire __net1340;
  wire __net1339;
  wire __net1338;
  wire __net1337;
  wire __net1336;
  wire __net1335;
  wire __net1334;
  wire __net1333;
  wire __net1332;
  wire __net1331;
  wire __net1330;
  wire __net1329;
  wire __net1328;
  wire __net1327;
  wire __net1326;
  wire __net1325;
  wire __net1324;
  wire __net1323;
  wire __net1322;
  wire __net1321;
  wire __net1320;
  wire __net1319;
  wire __net1318;
  wire __net1317;
  /* sub-modules */
  wire [4:0] _t0_in_fig0;
  wire [4:0] _t0_in_fig1;
  wire [16:0] _t0_in_val0;
  wire [16:0] _t0_in_val1;
  wire [16:0] _t0_val0;
  wire [16:0] _t0_val1;
  wire [4:0] _t0_fig0;
  wire [4:0] _t0_fig1;
  wire _t0_do;
  wire _t0_do2;
  wire _t0_p_reset;
  wire _t0_m_clock;
  wire [4:0] _t1_in_fig0;
  wire [4:0] _t1_in_fig1;
  wire [16:0] _t1_in_val0;
  wire [16:0] _t1_in_val1;
  wire [16:0] _t1_val0;
  wire [16:0] _t1_val1;
  wire [4:0] _t1_fig0;
  wire [4:0] _t1_fig1;
  wire _t1_do;
  wire _t1_do2;
  wire _t1_p_reset;
  wire _t1_m_clock;
  wire [4:0] _t2_in_fig0;
  wire [4:0] _t2_in_fig1;
  wire [16:0] _t2_in_val0;
  wire [16:0] _t2_in_val1;
  wire [16:0] _t2_val0;
  wire [16:0] _t2_val1;
  wire [4:0] _t2_fig0;
  wire [4:0] _t2_fig1;
  wire _t2_do;
  wire _t2_do2;
  wire _t2_p_reset;
  wire _t2_m_clock;
  wire [4:0] _t3_in_fig0;
  wire [4:0] _t3_in_fig1;
  wire [16:0] _t3_in_val0;
  wire [16:0] _t3_in_val1;
  wire [16:0] _t3_val0;
  wire [16:0] _t3_val1;
  wire [4:0] _t3_fig0;
  wire [4:0] _t3_fig1;
  wire _t3_do;
  wire _t3_do2;
  wire _t3_p_reset;
  wire _t3_m_clock;
  wire [4:0] _t4_in_fig0;
  wire [4:0] _t4_in_fig1;
  wire [16:0] _t4_in_val0;
  wire [16:0] _t4_in_val1;
  wire [16:0] _t4_val0;
  wire [16:0] _t4_val1;
  wire [4:0] _t4_fig0;
  wire [4:0] _t4_fig1;
  wire _t4_do;
  wire _t4_do2;
  wire _t4_p_reset;
  wire _t4_m_clock;
  wire [4:0] _t5_in_fig0;
  wire [4:0] _t5_in_fig1;
  wire [16:0] _t5_in_val0;
  wire [16:0] _t5_in_val1;
  wire [16:0] _t5_val0;
  wire [16:0] _t5_val1;
  wire [4:0] _t5_fig0;
  wire [4:0] _t5_fig1;
  wire _t5_do;
  wire _t5_do2;
  wire _t5_p_reset;
  wire _t5_m_clock;
  wire [4:0] _t6_in_fig0;
  wire [4:0] _t6_in_fig1;
  wire [16:0] _t6_in_val0;
  wire [16:0] _t6_in_val1;
  wire [16:0] _t6_val0;
  wire [16:0] _t6_val1;
  wire [4:0] _t6_fig0;
  wire [4:0] _t6_fig1;
  wire _t6_do;
  wire _t6_do2;
  wire _t6_p_reset;
  wire _t6_m_clock;
  wire [4:0] _t7_in_fig0;
  wire [4:0] _t7_in_fig1;
  wire [16:0] _t7_in_val0;
  wire [16:0] _t7_in_val1;
  wire [16:0] _t7_val0;
  wire [16:0] _t7_val1;
  wire [4:0] _t7_fig0;
  wire [4:0] _t7_fig1;
  wire _t7_do;
  wire _t7_do2;
  wire _t7_p_reset;
  wire _t7_m_clock;
  wire [4:0] _t8_in_fig0;
  wire [4:0] _t8_in_fig1;
  wire [16:0] _t8_in_val0;
  wire [16:0] _t8_in_val1;
  wire [16:0] _t8_val0;
  wire [16:0] _t8_val1;
  wire [4:0] _t8_fig0;
  wire [4:0] _t8_fig1;
  wire _t8_do;
  wire _t8_do2;
  wire _t8_p_reset;
  wire _t8_m_clock;
  wire [4:0] _t9_in_fig0;
  wire [4:0] _t9_in_fig1;
  wire [16:0] _t9_in_val0;
  wire [16:0] _t9_in_val1;
  wire [16:0] _t9_val0;
  wire [16:0] _t9_val1;
  wire [4:0] _t9_fig0;
  wire [4:0] _t9_fig1;
  wire _t9_do;
  wire _t9_do2;
  wire _t9_p_reset;
  wire _t9_m_clock;
  wire [4:0] _t10_in_fig0;
  wire [4:0] _t10_in_fig1;
  wire [16:0] _t10_in_val0;
  wire [16:0] _t10_in_val1;
  wire [16:0] _t10_val0;
  wire [16:0] _t10_val1;
  wire [4:0] _t10_fig0;
  wire [4:0] _t10_fig1;
  wire _t10_do;
  wire _t10_do2;
  wire _t10_p_reset;
  wire _t10_m_clock;
  wire [4:0] _t11_in_fig0;
  wire [4:0] _t11_in_fig1;
  wire [16:0] _t11_in_val0;
  wire [16:0] _t11_in_val1;
  wire [16:0] _t11_val0;
  wire [16:0] _t11_val1;
  wire [4:0] _t11_fig0;
  wire [4:0] _t11_fig1;
  wire _t11_do;
  wire _t11_do2;
  wire _t11_p_reset;
  wire _t11_m_clock;
  wire [4:0] _t12_in_fig0;
  wire [4:0] _t12_in_fig1;
  wire [16:0] _t12_in_val0;
  wire [16:0] _t12_in_val1;
  wire [16:0] _t12_val0;
  wire [16:0] _t12_val1;
  wire [4:0] _t12_fig0;
  wire [4:0] _t12_fig1;
  wire _t12_do;
  wire _t12_do2;
  wire _t12_p_reset;
  wire _t12_m_clock;
  wire [4:0] _t13_in_fig0;
  wire [4:0] _t13_in_fig1;
  wire [16:0] _t13_in_val0;
  wire [16:0] _t13_in_val1;
  wire [16:0] _t13_val0;
  wire [16:0] _t13_val1;
  wire [4:0] _t13_fig0;
  wire [4:0] _t13_fig1;
  wire _t13_do;
  wire _t13_do2;
  wire _t13_p_reset;
  wire _t13_m_clock;
  wire [4:0] _t14_in_fig0;
  wire [4:0] _t14_in_fig1;
  wire [16:0] _t14_in_val0;
  wire [16:0] _t14_in_val1;
  wire [16:0] _t14_val0;
  wire [16:0] _t14_val1;
  wire [4:0] _t14_fig0;
  wire [4:0] _t14_fig1;
  wire _t14_do;
  wire _t14_do2;
  wire _t14_p_reset;
  wire _t14_m_clock;
  wire [4:0] _t15_in_fig0;
  wire [4:0] _t15_in_fig1;
  wire [16:0] _t15_in_val0;
  wire [16:0] _t15_in_val1;
  wire [16:0] _t15_val0;
  wire [16:0] _t15_val1;
  wire [4:0] _t15_fig0;
  wire [4:0] _t15_fig1;
  wire _t15_do;
  wire _t15_do2;
  wire _t15_p_reset;
  wire _t15_m_clock;
  wire [4:0] _t16_in_fig0;
  wire [4:0] _t16_in_fig1;
  wire [16:0] _t16_in_val0;
  wire [16:0] _t16_in_val1;
  wire [16:0] _t16_val0;
  wire [16:0] _t16_val1;
  wire [4:0] _t16_fig0;
  wire [4:0] _t16_fig1;
  wire _t16_do;
  wire _t16_do2;
  wire _t16_p_reset;
  wire _t16_m_clock;
  wire [4:0] _t17_in_fig0;
  wire [4:0] _t17_in_fig1;
  wire [16:0] _t17_in_val0;
  wire [16:0] _t17_in_val1;
  wire [16:0] _t17_val0;
  wire [16:0] _t17_val1;
  wire [4:0] _t17_fig0;
  wire [4:0] _t17_fig1;
  wire _t17_do;
  wire _t17_do2;
  wire _t17_p_reset;
  wire _t17_m_clock;
  wire [4:0] _t18_in_fig0;
  wire [4:0] _t18_in_fig1;
  wire [16:0] _t18_in_val0;
  wire [16:0] _t18_in_val1;
  wire [16:0] _t18_val0;
  wire [16:0] _t18_val1;
  wire [4:0] _t18_fig0;
  wire [4:0] _t18_fig1;
  wire _t18_do;
  wire _t18_do2;
  wire _t18_p_reset;
  wire _t18_m_clock;
  wire [4:0] _t19_in_fig0;
  wire [4:0] _t19_in_fig1;
  wire [16:0] _t19_in_val0;
  wire [16:0] _t19_in_val1;
  wire [16:0] _t19_val0;
  wire [16:0] _t19_val1;
  wire [4:0] _t19_fig0;
  wire [4:0] _t19_fig1;
  wire _t19_do;
  wire _t19_do2;
  wire _t19_p_reset;
  wire _t19_m_clock;
  wire [4:0] _t20_in_fig0;
  wire [4:0] _t20_in_fig1;
  wire [16:0] _t20_in_val0;
  wire [16:0] _t20_in_val1;
  wire [16:0] _t20_val0;
  wire [16:0] _t20_val1;
  wire [4:0] _t20_fig0;
  wire [4:0] _t20_fig1;
  wire _t20_do;
  wire _t20_do2;
  wire _t20_p_reset;
  wire _t20_m_clock;
  wire [4:0] _t21_in_fig0;
  wire [4:0] _t21_in_fig1;
  wire [16:0] _t21_in_val0;
  wire [16:0] _t21_in_val1;
  wire [16:0] _t21_val0;
  wire [16:0] _t21_val1;
  wire [4:0] _t21_fig0;
  wire [4:0] _t21_fig1;
  wire _t21_do;
  wire _t21_do2;
  wire _t21_p_reset;
  wire _t21_m_clock;
  wire [4:0] _t22_in_fig0;
  wire [4:0] _t22_in_fig1;
  wire [16:0] _t22_in_val0;
  wire [16:0] _t22_in_val1;
  wire [16:0] _t22_val0;
  wire [16:0] _t22_val1;
  wire [4:0] _t22_fig0;
  wire [4:0] _t22_fig1;
  wire _t22_do;
  wire _t22_do2;
  wire _t22_p_reset;
  wire _t22_m_clock;
  wire [4:0] _t23_in_fig0;
  wire [4:0] _t23_in_fig1;
  wire [16:0] _t23_in_val0;
  wire [16:0] _t23_in_val1;
  wire [16:0] _t23_val0;
  wire [16:0] _t23_val1;
  wire [4:0] _t23_fig0;
  wire [4:0] _t23_fig1;
  wire _t23_do;
  wire _t23_do2;
  wire _t23_p_reset;
  wire _t23_m_clock;
  wire [4:0] _t24_in_fig0;
  wire [4:0] _t24_in_fig1;
  wire [16:0] _t24_in_val0;
  wire [16:0] _t24_in_val1;
  wire [16:0] _t24_val0;
  wire [16:0] _t24_val1;
  wire [4:0] _t24_fig0;
  wire [4:0] _t24_fig1;
  wire _t24_do;
  wire _t24_do2;
  wire _t24_p_reset;
  wire _t24_m_clock;
  wire [4:0] _t25_in_fig0;
  wire [4:0] _t25_in_fig1;
  wire [16:0] _t25_in_val0;
  wire [16:0] _t25_in_val1;
  wire [16:0] _t25_val0;
  wire [16:0] _t25_val1;
  wire [4:0] _t25_fig0;
  wire [4:0] _t25_fig1;
  wire _t25_do;
  wire _t25_do2;
  wire _t25_p_reset;
  wire _t25_m_clock;
  wire [4:0] _t26_in_fig0;
  wire [4:0] _t26_in_fig1;
  wire [16:0] _t26_in_val0;
  wire [16:0] _t26_in_val1;
  wire [16:0] _t26_val0;
  wire [16:0] _t26_val1;
  wire [4:0] _t26_fig0;
  wire [4:0] _t26_fig1;
  wire _t26_do;
  wire _t26_do2;
  wire _t26_p_reset;
  wire _t26_m_clock;
  wire [4:0] _t27_in_fig0;
  wire [4:0] _t27_in_fig1;
  wire [16:0] _t27_in_val0;
  wire [16:0] _t27_in_val1;
  wire [16:0] _t27_val0;
  wire [16:0] _t27_val1;
  wire [4:0] _t27_fig0;
  wire [4:0] _t27_fig1;
  wire _t27_do;
  wire _t27_do2;
  wire _t27_p_reset;
  wire _t27_m_clock;
  wire [4:0] _t28_in_fig0;
  wire [4:0] _t28_in_fig1;
  wire [16:0] _t28_in_val0;
  wire [16:0] _t28_in_val1;
  wire [16:0] _t28_val0;
  wire [16:0] _t28_val1;
  wire [4:0] _t28_fig0;
  wire [4:0] _t28_fig1;
  wire _t28_do;
  wire _t28_do2;
  wire _t28_p_reset;
  wire _t28_m_clock;
  wire [4:0] _t29_in_fig0;
  wire [4:0] _t29_in_fig1;
  wire [16:0] _t29_in_val0;
  wire [16:0] _t29_in_val1;
  wire [16:0] _t29_val0;
  wire [16:0] _t29_val1;
  wire [4:0] _t29_fig0;
  wire [4:0] _t29_fig1;
  wire _t29_do;
  wire _t29_do2;
  wire _t29_p_reset;
  wire _t29_m_clock;
  wire [4:0] _t30_in_fig0;
  wire [4:0] _t30_in_fig1;
  wire [16:0] _t30_in_val0;
  wire [16:0] _t30_in_val1;
  wire [16:0] _t30_val0;
  wire [16:0] _t30_val1;
  wire [4:0] _t30_fig0;
  wire [4:0] _t30_fig1;
  wire _t30_do;
  wire _t30_do2;
  wire _t30_p_reset;
  wire _t30_m_clock;
  wire [4:0] _t31_in_fig0;
  wire [4:0] _t31_in_fig1;
  wire [16:0] _t31_in_val0;
  wire [16:0] _t31_in_val1;
  wire [16:0] _t31_val0;
  wire [16:0] _t31_val1;
  wire [4:0] _t31_fig0;
  wire [4:0] _t31_fig1;
  wire _t31_do;
  wire _t31_do2;
  wire _t31_p_reset;
  wire _t31_m_clock;
  wire [4:0] _t32_in_fig0;
  wire [4:0] _t32_in_fig1;
  wire [16:0] _t32_in_val0;
  wire [16:0] _t32_in_val1;
  wire [16:0] _t32_val0;
  wire [16:0] _t32_val1;
  wire [4:0] _t32_fig0;
  wire [4:0] _t32_fig1;
  wire _t32_do;
  wire _t32_do2;
  wire _t32_p_reset;
  wire _t32_m_clock;
  wire [4:0] _t33_in_fig0;
  wire [4:0] _t33_in_fig1;
  wire [16:0] _t33_in_val0;
  wire [16:0] _t33_in_val1;
  wire [16:0] _t33_val0;
  wire [16:0] _t33_val1;
  wire [4:0] _t33_fig0;
  wire [4:0] _t33_fig1;
  wire _t33_do;
  wire _t33_do2;
  wire _t33_p_reset;
  wire _t33_m_clock;
  tournament1 t0(.p_reset(_t0_p_reset),
                 .m_clock(_t0_m_clock),
                 .in_fig0(_t0_in_fig0),
                 .in_fig1(_t0_in_fig1),
                 .in_val0(_t0_in_val0),
                 .in_val1(_t0_in_val1),
                 .val0(_t0_val0),
                 .val1(_t0_val1),
                 .fig0(_t0_fig0),
                 .fig1(_t0_fig1),
                 .do(_t0_do),
                 .do2(_t0_do2));
  tournament1 t1(.p_reset(_t1_p_reset),
                 .m_clock(_t1_m_clock),
                 .in_fig0(_t1_in_fig0),
                 .in_fig1(_t1_in_fig1),
                 .in_val0(_t1_in_val0),
                 .in_val1(_t1_in_val1),
                 .val0(_t1_val0),
                 .val1(_t1_val1),
                 .fig0(_t1_fig0),
                 .fig1(_t1_fig1),
                 .do(_t1_do),
                 .do2(_t1_do2));
  tournament1 t2(.p_reset(_t2_p_reset),
                 .m_clock(_t2_m_clock),
                 .in_fig0(_t2_in_fig0),
                 .in_fig1(_t2_in_fig1),
                 .in_val0(_t2_in_val0),
                 .in_val1(_t2_in_val1),
                 .val0(_t2_val0),
                 .val1(_t2_val1),
                 .fig0(_t2_fig0),
                 .fig1(_t2_fig1),
                 .do(_t2_do),
                 .do2(_t2_do2));
  tournament1 t3(.p_reset(_t3_p_reset),
                 .m_clock(_t3_m_clock),
                 .in_fig0(_t3_in_fig0),
                 .in_fig1(_t3_in_fig1),
                 .in_val0(_t3_in_val0),
                 .in_val1(_t3_in_val1),
                 .val0(_t3_val0),
                 .val1(_t3_val1),
                 .fig0(_t3_fig0),
                 .fig1(_t3_fig1),
                 .do(_t3_do),
                 .do2(_t3_do2));
  tournament1 t4(.p_reset(_t4_p_reset),
                 .m_clock(_t4_m_clock),
                 .in_fig0(_t4_in_fig0),
                 .in_fig1(_t4_in_fig1),
                 .in_val0(_t4_in_val0),
                 .in_val1(_t4_in_val1),
                 .val0(_t4_val0),
                 .val1(_t4_val1),
                 .fig0(_t4_fig0),
                 .fig1(_t4_fig1),
                 .do(_t4_do),
                 .do2(_t4_do2));
  tournament1 t5(.p_reset(_t5_p_reset),
                 .m_clock(_t5_m_clock),
                 .in_fig0(_t5_in_fig0),
                 .in_fig1(_t5_in_fig1),
                 .in_val0(_t5_in_val0),
                 .in_val1(_t5_in_val1),
                 .val0(_t5_val0),
                 .val1(_t5_val1),
                 .fig0(_t5_fig0),
                 .fig1(_t5_fig1),
                 .do(_t5_do),
                 .do2(_t5_do2));
  tournament1 t6(.p_reset(_t6_p_reset),
                 .m_clock(_t6_m_clock),
                 .in_fig0(_t6_in_fig0),
                 .in_fig1(_t6_in_fig1),
                 .in_val0(_t6_in_val0),
                 .in_val1(_t6_in_val1),
                 .val0(_t6_val0),
                 .val1(_t6_val1),
                 .fig0(_t6_fig0),
                 .fig1(_t6_fig1),
                 .do(_t6_do),
                 .do2(_t6_do2));
  tournament1 t7(.p_reset(_t7_p_reset),
                 .m_clock(_t7_m_clock),
                 .in_fig0(_t7_in_fig0),
                 .in_fig1(_t7_in_fig1),
                 .in_val0(_t7_in_val0),
                 .in_val1(_t7_in_val1),
                 .val0(_t7_val0),
                 .val1(_t7_val1),
                 .fig0(_t7_fig0),
                 .fig1(_t7_fig1),
                 .do(_t7_do),
                 .do2(_t7_do2));
  tournament1 t8(.p_reset(_t8_p_reset),
                 .m_clock(_t8_m_clock),
                 .in_fig0(_t8_in_fig0),
                 .in_fig1(_t8_in_fig1),
                 .in_val0(_t8_in_val0),
                 .in_val1(_t8_in_val1),
                 .val0(_t8_val0),
                 .val1(_t8_val1),
                 .fig0(_t8_fig0),
                 .fig1(_t8_fig1),
                 .do(_t8_do),
                 .do2(_t8_do2));
  tournament1 t9(.p_reset(_t9_p_reset),
                 .m_clock(_t9_m_clock),
                 .in_fig0(_t9_in_fig0),
                 .in_fig1(_t9_in_fig1),
                 .in_val0(_t9_in_val0),
                 .in_val1(_t9_in_val1),
                 .val0(_t9_val0),
                 .val1(_t9_val1),
                 .fig0(_t9_fig0),
                 .fig1(_t9_fig1),
                 .do(_t9_do),
                 .do2(_t9_do2));
  tournament1 t10(.p_reset(_t10_p_reset),
                  .m_clock(_t10_m_clock),
                  .in_fig0(_t10_in_fig0),
                  .in_fig1(_t10_in_fig1),
                  .in_val0(_t10_in_val0),
                  .in_val1(_t10_in_val1),
                  .val0(_t10_val0),
                  .val1(_t10_val1),
                  .fig0(_t10_fig0),
                  .fig1(_t10_fig1),
                  .do(_t10_do),
                  .do2(_t10_do2));
  tournament1 t11(.p_reset(_t11_p_reset),
                  .m_clock(_t11_m_clock),
                  .in_fig0(_t11_in_fig0),
                  .in_fig1(_t11_in_fig1),
                  .in_val0(_t11_in_val0),
                  .in_val1(_t11_in_val1),
                  .val0(_t11_val0),
                  .val1(_t11_val1),
                  .fig0(_t11_fig0),
                  .fig1(_t11_fig1),
                  .do(_t11_do),
                  .do2(_t11_do2));
  tournament1 t12(.p_reset(_t12_p_reset),
                  .m_clock(_t12_m_clock),
                  .in_fig0(_t12_in_fig0),
                  .in_fig1(_t12_in_fig1),
                  .in_val0(_t12_in_val0),
                  .in_val1(_t12_in_val1),
                  .val0(_t12_val0),
                  .val1(_t12_val1),
                  .fig0(_t12_fig0),
                  .fig1(_t12_fig1),
                  .do(_t12_do),
                  .do2(_t12_do2));
  tournament1 t13(.p_reset(_t13_p_reset),
                  .m_clock(_t13_m_clock),
                  .in_fig0(_t13_in_fig0),
                  .in_fig1(_t13_in_fig1),
                  .in_val0(_t13_in_val0),
                  .in_val1(_t13_in_val1),
                  .val0(_t13_val0),
                  .val1(_t13_val1),
                  .fig0(_t13_fig0),
                  .fig1(_t13_fig1),
                  .do(_t13_do),
                  .do2(_t13_do2));
  tournament1 t14(.p_reset(_t14_p_reset),
                  .m_clock(_t14_m_clock),
                  .in_fig0(_t14_in_fig0),
                  .in_fig1(_t14_in_fig1),
                  .in_val0(_t14_in_val0),
                  .in_val1(_t14_in_val1),
                  .val0(_t14_val0),
                  .val1(_t14_val1),
                  .fig0(_t14_fig0),
                  .fig1(_t14_fig1),
                  .do(_t14_do),
                  .do2(_t14_do2));
  tournament1 t15(.p_reset(_t15_p_reset),
                  .m_clock(_t15_m_clock),
                  .in_fig0(_t15_in_fig0),
                  .in_fig1(_t15_in_fig1),
                  .in_val0(_t15_in_val0),
                  .in_val1(_t15_in_val1),
                  .val0(_t15_val0),
                  .val1(_t15_val1),
                  .fig0(_t15_fig0),
                  .fig1(_t15_fig1),
                  .do(_t15_do),
                  .do2(_t15_do2));
  tournament1 t16(.p_reset(_t16_p_reset),
                  .m_clock(_t16_m_clock),
                  .in_fig0(_t16_in_fig0),
                  .in_fig1(_t16_in_fig1),
                  .in_val0(_t16_in_val0),
                  .in_val1(_t16_in_val1),
                  .val0(_t16_val0),
                  .val1(_t16_val1),
                  .fig0(_t16_fig0),
                  .fig1(_t16_fig1),
                  .do(_t16_do),
                  .do2(_t16_do2));
  tournament1 t17(.p_reset(_t17_p_reset),
                  .m_clock(_t17_m_clock),
                  .in_fig0(_t17_in_fig0),
                  .in_fig1(_t17_in_fig1),
                  .in_val0(_t17_in_val0),
                  .in_val1(_t17_in_val1),
                  .val0(_t17_val0),
                  .val1(_t17_val1),
                  .fig0(_t17_fig0),
                  .fig1(_t17_fig1),
                  .do(_t17_do),
                  .do2(_t17_do2));
  tournament1 t18(.p_reset(_t18_p_reset),
                  .m_clock(_t18_m_clock),
                  .in_fig0(_t18_in_fig0),
                  .in_fig1(_t18_in_fig1),
                  .in_val0(_t18_in_val0),
                  .in_val1(_t18_in_val1),
                  .val0(_t18_val0),
                  .val1(_t18_val1),
                  .fig0(_t18_fig0),
                  .fig1(_t18_fig1),
                  .do(_t18_do),
                  .do2(_t18_do2));
  tournament1 t19(.p_reset(_t19_p_reset),
                  .m_clock(_t19_m_clock),
                  .in_fig0(_t19_in_fig0),
                  .in_fig1(_t19_in_fig1),
                  .in_val0(_t19_in_val0),
                  .in_val1(_t19_in_val1),
                  .val0(_t19_val0),
                  .val1(_t19_val1),
                  .fig0(_t19_fig0),
                  .fig1(_t19_fig1),
                  .do(_t19_do),
                  .do2(_t19_do2));
  tournament1 t20(.p_reset(_t20_p_reset),
                  .m_clock(_t20_m_clock),
                  .in_fig0(_t20_in_fig0),
                  .in_fig1(_t20_in_fig1),
                  .in_val0(_t20_in_val0),
                  .in_val1(_t20_in_val1),
                  .val0(_t20_val0),
                  .val1(_t20_val1),
                  .fig0(_t20_fig0),
                  .fig1(_t20_fig1),
                  .do(_t20_do),
                  .do2(_t20_do2));
  tournament1 t21(.p_reset(_t21_p_reset),
                  .m_clock(_t21_m_clock),
                  .in_fig0(_t21_in_fig0),
                  .in_fig1(_t21_in_fig1),
                  .in_val0(_t21_in_val0),
                  .in_val1(_t21_in_val1),
                  .val0(_t21_val0),
                  .val1(_t21_val1),
                  .fig0(_t21_fig0),
                  .fig1(_t21_fig1),
                  .do(_t21_do),
                  .do2(_t21_do2));
  tournament1 t22(.p_reset(_t22_p_reset),
                  .m_clock(_t22_m_clock),
                  .in_fig0(_t22_in_fig0),
                  .in_fig1(_t22_in_fig1),
                  .in_val0(_t22_in_val0),
                  .in_val1(_t22_in_val1),
                  .val0(_t22_val0),
                  .val1(_t22_val1),
                  .fig0(_t22_fig0),
                  .fig1(_t22_fig1),
                  .do(_t22_do),
                  .do2(_t22_do2));
  tournament1 t23(.p_reset(_t23_p_reset),
                  .m_clock(_t23_m_clock),
                  .in_fig0(_t23_in_fig0),
                  .in_fig1(_t23_in_fig1),
                  .in_val0(_t23_in_val0),
                  .in_val1(_t23_in_val1),
                  .val0(_t23_val0),
                  .val1(_t23_val1),
                  .fig0(_t23_fig0),
                  .fig1(_t23_fig1),
                  .do(_t23_do),
                  .do2(_t23_do2));
  tournament1 t24(.p_reset(_t24_p_reset),
                  .m_clock(_t24_m_clock),
                  .in_fig0(_t24_in_fig0),
                  .in_fig1(_t24_in_fig1),
                  .in_val0(_t24_in_val0),
                  .in_val1(_t24_in_val1),
                  .val0(_t24_val0),
                  .val1(_t24_val1),
                  .fig0(_t24_fig0),
                  .fig1(_t24_fig1),
                  .do(_t24_do),
                  .do2(_t24_do2));
  tournament1 t25(.p_reset(_t25_p_reset),
                  .m_clock(_t25_m_clock),
                  .in_fig0(_t25_in_fig0),
                  .in_fig1(_t25_in_fig1),
                  .in_val0(_t25_in_val0),
                  .in_val1(_t25_in_val1),
                  .val0(_t25_val0),
                  .val1(_t25_val1),
                  .fig0(_t25_fig0),
                  .fig1(_t25_fig1),
                  .do(_t25_do),
                  .do2(_t25_do2));
  tournament1 t26(.p_reset(_t26_p_reset),
                  .m_clock(_t26_m_clock),
                  .in_fig0(_t26_in_fig0),
                  .in_fig1(_t26_in_fig1),
                  .in_val0(_t26_in_val0),
                  .in_val1(_t26_in_val1),
                  .val0(_t26_val0),
                  .val1(_t26_val1),
                  .fig0(_t26_fig0),
                  .fig1(_t26_fig1),
                  .do(_t26_do),
                  .do2(_t26_do2));
  tournament1 t27(.p_reset(_t27_p_reset),
                  .m_clock(_t27_m_clock),
                  .in_fig0(_t27_in_fig0),
                  .in_fig1(_t27_in_fig1),
                  .in_val0(_t27_in_val0),
                  .in_val1(_t27_in_val1),
                  .val0(_t27_val0),
                  .val1(_t27_val1),
                  .fig0(_t27_fig0),
                  .fig1(_t27_fig1),
                  .do(_t27_do),
                  .do2(_t27_do2));
  tournament1 t28(.p_reset(_t28_p_reset),
                  .m_clock(_t28_m_clock),
                  .in_fig0(_t28_in_fig0),
                  .in_fig1(_t28_in_fig1),
                  .in_val0(_t28_in_val0),
                  .in_val1(_t28_in_val1),
                  .val0(_t28_val0),
                  .val1(_t28_val1),
                  .fig0(_t28_fig0),
                  .fig1(_t28_fig1),
                  .do(_t28_do),
                  .do2(_t28_do2));
  tournament1 t29(.p_reset(_t29_p_reset),
                  .m_clock(_t29_m_clock),
                  .in_fig0(_t29_in_fig0),
                  .in_fig1(_t29_in_fig1),
                  .in_val0(_t29_in_val0),
                  .in_val1(_t29_in_val1),
                  .val0(_t29_val0),
                  .val1(_t29_val1),
                  .fig0(_t29_fig0),
                  .fig1(_t29_fig1),
                  .do(_t29_do),
                  .do2(_t29_do2));
  tournament1 t30(.p_reset(_t30_p_reset),
                  .m_clock(_t30_m_clock),
                  .in_fig0(_t30_in_fig0),
                  .in_fig1(_t30_in_fig1),
                  .in_val0(_t30_in_val0),
                  .in_val1(_t30_in_val1),
                  .val0(_t30_val0),
                  .val1(_t30_val1),
                  .fig0(_t30_fig0),
                  .fig1(_t30_fig1),
                  .do(_t30_do),
                  .do2(_t30_do2));
  tournament1 t31(.p_reset(_t31_p_reset),
                  .m_clock(_t31_m_clock),
                  .in_fig0(_t31_in_fig0),
                  .in_fig1(_t31_in_fig1),
                  .in_val0(_t31_in_val0),
                  .in_val1(_t31_in_val1),
                  .val0(_t31_val0),
                  .val1(_t31_val1),
                  .fig0(_t31_fig0),
                  .fig1(_t31_fig1),
                  .do(_t31_do),
                  .do2(_t31_do2));
  tournament1 t32(.p_reset(_t32_p_reset),
                  .m_clock(_t32_m_clock),
                  .in_fig0(_t32_in_fig0),
                  .in_fig1(_t32_in_fig1),
                  .in_val0(_t32_in_val0),
                  .in_val1(_t32_in_val1),
                  .val0(_t32_val0),
                  .val1(_t32_val1),
                  .fig0(_t32_fig0),
                  .fig1(_t32_fig1),
                  .do(_t32_do),
                  .do2(_t32_do2));
  tournament1 t33(.p_reset(_t33_p_reset),
                  .m_clock(_t33_m_clock),
                  .in_fig0(_t33_in_fig0),
                  .in_fig1(_t33_in_fig1),
                  .in_val0(_t33_in_val0),
                  .in_val1(_t33_in_val1),
                  .val0(_t33_val0),
                  .val1(_t33_val1),
                  .fig0(_t33_fig0),
                  .fig1(_t33_fig1),
                  .do(_t33_do),
                  .do2(_t33_do2));
  /* actions */
  assign __net1317 = r0out[4:0] == 5'b00000;
  assign __net1318 = r0out[4:0] == 5'b00001;
  assign __net1319 = r0out[4:0] == 5'b00010;
  assign __net1320 = r0out[4:0] == 5'b00011;
  assign __net1321 = r0out[4:0] == 5'b00100;
  assign __net1322 = r0out[4:0] == 5'b00101;
  assign __net1323 = r0out[4:0] == 5'b00110;
  assign __net1324 = r0out[4:0] == 5'b00111;
  assign __net1325 = r0out[4:0] == 5'b01000;
  assign __net1326 = r0out[4:0] == 5'b01001;
  assign __net1327 = r0out[4:0] == 5'b01010;
  assign __net1328 = r0out[4:0] == 5'b01011;
  assign __net1329 = r0out[4:0] == 5'b01100;
  assign __net1330 = r0out[4:0] == 5'b01101;
  assign __net1331 = r0out[4:0] == 5'b01110;
  assign __net1332 = r0out[4:0] == 5'b01111;
  assign __net1333 = r0out[4:0] == 5'b10000;
  assign __net1334 = r0out[4:0] == 5'b10001;
  assign __net1335 = r0out[4:0] == 5'b10010;
  assign __net1336 = r0out[4:0] == 5'b10011;
  assign __net1337 = r0out[4:0] == 5'b10100;
  assign __net1338 = r0out[4:0] == 5'b10101;
  assign __net1339 = r0out[4:0] == 5'b10110;
  assign __net1340 = r0out[4:0] == 5'b10111;
  assign __net1341 = r0out[4:0] == 5'b11000;
  assign __net1342 = r0out[4:0] == 5'b11001;
  assign __net1343 = r0out[4:0] == 5'b11010;
  assign __net1344 = r0out[4:0] == 5'b11011;
  assign __net1345 = r0out[4:0] == 5'b11100;
  assign __net1346 = r0out[4:0] == 5'b11101;
  assign __net1347 = r0out[4:0] == 5'b11110;
  assign __net1348 = r0out[4:0] == 5'b11111;
  assign __net1349 = r0out[9:5] == 5'b00000;
  assign __net1350 = r0out[9:5] == 5'b00001;
  assign __net1351 = r0out[9:5] == 5'b00010;
  assign __net1352 = r0out[9:5] == 5'b00011;
  assign __net1353 = r0out[9:5] == 5'b00100;
  assign __net1354 = r0out[9:5] == 5'b00101;
  assign __net1355 = r0out[9:5] == 5'b00110;
  assign __net1356 = r0out[9:5] == 5'b00111;
  assign __net1357 = r0out[9:5] == 5'b01000;
  assign __net1358 = r0out[9:5] == 5'b01001;
  assign __net1359 = r0out[9:5] == 5'b01010;
  assign __net1360 = r0out[9:5] == 5'b01011;
  assign __net1361 = r0out[9:5] == 5'b01100;
  assign __net1362 = r0out[9:5] == 5'b01101;
  assign __net1363 = r0out[9:5] == 5'b01110;
  assign __net1364 = r0out[9:5] == 5'b01111;
  assign __net1365 = r0out[9:5] == 5'b10000;
  assign __net1366 = r0out[9:5] == 5'b10001;
  assign __net1367 = r0out[9:5] == 5'b10010;
  assign __net1368 = r0out[9:5] == 5'b10011;
  assign __net1369 = r0out[9:5] == 5'b10100;
  assign __net1370 = r0out[9:5] == 5'b10101;
  assign __net1371 = r0out[9:5] == 5'b10110;
  assign __net1372 = r0out[9:5] == 5'b10111;
  assign __net1373 = r0out[9:5] == 5'b11000;
  assign __net1374 = r0out[9:5] == 5'b11001;
  assign __net1375 = r0out[9:5] == 5'b11010;
  assign __net1376 = r0out[9:5] == 5'b11011;
  assign __net1377 = r0out[9:5] == 5'b11100;
  assign __net1378 = r0out[9:5] == 5'b11101;
  assign __net1379 = r0out[9:5] == 5'b11110;
  assign __net1380 = r0out[9:5] == 5'b11111;
  assign __net1381 = r0out[14:10] == 5'b00000;
  assign __net1382 = r0out[14:10] == 5'b00001;
  assign __net1383 = r0out[14:10] == 5'b00010;
  assign __net1384 = r0out[14:10] == 5'b00011;
  assign __net1385 = r0out[14:10] == 5'b00100;
  assign __net1386 = r0out[14:10] == 5'b00101;
  assign __net1387 = r0out[14:10] == 5'b00110;
  assign __net1388 = r0out[14:10] == 5'b00111;
  assign __net1389 = r0out[14:10] == 5'b01000;
  assign __net1390 = r0out[14:10] == 5'b01001;
  assign __net1391 = r0out[14:10] == 5'b01010;
  assign __net1392 = r0out[14:10] == 5'b01011;
  assign __net1393 = r0out[14:10] == 5'b01100;
  assign __net1394 = r0out[14:10] == 5'b01101;
  assign __net1395 = r0out[14:10] == 5'b01110;
  assign __net1396 = r0out[14:10] == 5'b01111;
  assign __net1397 = r0out[14:10] == 5'b10000;
  assign __net1398 = r0out[14:10] == 5'b10001;
  assign __net1399 = r0out[14:10] == 5'b10010;
  assign __net1400 = r0out[14:10] == 5'b10011;
  assign __net1401 = r0out[14:10] == 5'b10100;
  assign __net1402 = r0out[14:10] == 5'b10101;
  assign __net1403 = r0out[14:10] == 5'b10110;
  assign __net1404 = r0out[14:10] == 5'b10111;
  assign __net1405 = r0out[14:10] == 5'b11000;
  assign __net1406 = r0out[14:10] == 5'b11001;
  assign __net1407 = r0out[14:10] == 5'b11010;
  assign __net1408 = r0out[14:10] == 5'b11011;
  assign __net1409 = r0out[14:10] == 5'b11100;
  assign __net1410 = r0out[14:10] == 5'b11101;
  assign __net1411 = r0out[14:10] == 5'b11110;
  assign __net1412 = r0out[14:10] == 5'b11111;
  assign __net1413 = r0out[19:15] == 5'b00000;
  assign __net1414 = r0out[19:15] == 5'b00001;
  assign __net1415 = r0out[19:15] == 5'b00010;
  assign __net1416 = r0out[19:15] == 5'b00011;
  assign __net1417 = r0out[19:15] == 5'b00100;
  assign __net1418 = r0out[19:15] == 5'b00101;
  assign __net1419 = r0out[19:15] == 5'b00110;
  assign __net1420 = r0out[19:15] == 5'b00111;
  assign __net1421 = r0out[19:15] == 5'b01000;
  assign __net1422 = r0out[19:15] == 5'b01001;
  assign __net1423 = r0out[19:15] == 5'b01010;
  assign __net1424 = r0out[19:15] == 5'b01011;
  assign __net1425 = r0out[19:15] == 5'b01100;
  assign __net1426 = r0out[19:15] == 5'b01101;
  assign __net1427 = r0out[19:15] == 5'b01110;
  assign __net1428 = r0out[19:15] == 5'b01111;
  assign __net1429 = r0out[19:15] == 5'b10000;
  assign __net1430 = r0out[19:15] == 5'b10001;
  assign __net1431 = r0out[19:15] == 5'b10010;
  assign __net1432 = r0out[19:15] == 5'b10011;
  assign __net1433 = r0out[19:15] == 5'b10100;
  assign __net1434 = r0out[19:15] == 5'b10101;
  assign __net1435 = r0out[19:15] == 5'b10110;
  assign __net1436 = r0out[19:15] == 5'b10111;
  assign __net1437 = r0out[19:15] == 5'b11000;
  assign __net1438 = r0out[19:15] == 5'b11001;
  assign __net1439 = r0out[19:15] == 5'b11010;
  assign __net1440 = r0out[19:15] == 5'b11011;
  assign __net1441 = r0out[19:15] == 5'b11100;
  assign __net1442 = r0out[19:15] == 5'b11101;
  assign __net1443 = r0out[19:15] == 5'b11110;
  assign __net1444 = r0out[19:15] == 5'b11111;
  assign __net1445 = write ? num == 5'b00000 : 1'bx;
  assign __net1446 = write ? num == 5'b00001 : 1'bx;
  assign __net1447 = write ? num == 5'b00010 : 1'bx;
  assign __net1448 = write ? num == 5'b00011 : 1'bx;
  assign __net1449 = write ? num == 5'b00100 : 1'bx;
  assign __net1450 = write ? num == 5'b00101 : 1'bx;
  assign __net1451 = write ? num == 5'b00110 : 1'bx;
  assign __net1452 = write ? num == 5'b00111 : 1'bx;
  assign __net1453 = write ? num == 5'b01000 : 1'bx;
  assign __net1454 = write ? num == 5'b01001 : 1'bx;
  assign __net1455 = write ? num == 5'b01010 : 1'bx;
  assign __net1456 = write ? num == 5'b01011 : 1'bx;
  assign __net1457 = write ? num == 5'b01100 : 1'bx;
  assign __net1458 = write ? num == 5'b01101 : 1'bx;
  assign __net1459 = write ? num == 5'b01110 : 1'bx;
  assign __net1460 = write ? num == 5'b01111 : 1'bx;
  assign __net1461 = write ? num == 5'b10000 : 1'bx;
  assign __net1462 = write ? num == 5'b10001 : 1'bx;
  assign __net1463 = write ? num == 5'b10010 : 1'bx;
  assign __net1464 = write ? num == 5'b10011 : 1'bx;
  assign __net1465 = write ? num == 5'b10100 : 1'bx;
  assign __net1466 = write ? num == 5'b10101 : 1'bx;
  assign __net1467 = write ? num == 5'b10110 : 1'bx;
  assign __net1468 = write ? num == 5'b10111 : 1'bx;
  assign __net1469 = write ? num == 5'b11000 : 1'bx;
  assign __net1470 = write ? num == 5'b11001 : 1'bx;
  assign __net1471 = write ? num == 5'b11010 : 1'bx;
  assign __net1472 = write ? num == 5'b11011 : 1'bx;
  assign __net1473 = write ? num == 5'b11100 : 1'bx;
  assign __net1474 = write ? num == 5'b11101 : 1'bx;
  assign __net1475 = write ? num == 5'b11110 : 1'bx;
  assign __net1476 = write ? num == 5'b11111 : 1'bx;
  /* invokes */
  assign _t1_p_reset = p_reset;
  assign _t1_m_clock = m_clock;
  assign _t1_do = tournament;
  assign _t4_p_reset = p_reset;
  assign _t4_m_clock = m_clock;
  assign _t4_do = tournament;
  assign _t32_p_reset = p_reset;
  assign _t32_m_clock = m_clock;
  assign _t32_do2 = __stage_COMP;
  assign _t11_p_reset = p_reset;
  assign _t11_m_clock = m_clock;
  assign _t11_do = tournament;
  assign _t26_p_reset = p_reset;
  assign _t26_m_clock = m_clock;
  assign _t26_do = tournament;
  assign _t29_p_reset = p_reset;
  assign _t29_m_clock = m_clock;
  assign _t29_do = tournament;
  assign _t14_p_reset = p_reset;
  assign _t14_m_clock = m_clock;
  assign _t14_do = tournament;
  assign _t20_p_reset = p_reset;
  assign _t20_m_clock = m_clock;
  assign _t20_do = tournament;
  assign _t17_p_reset = p_reset;
  assign _t17_m_clock = m_clock;
  assign _t17_do = tournament;
  assign _t23_p_reset = p_reset;
  assign _t23_m_clock = m_clock;
  assign _t23_do = tournament;
  assign _t6_p_reset = p_reset;
  assign _t6_m_clock = m_clock;
  assign _t6_do = tournament;
  assign _t9_p_reset = p_reset;
  assign _t9_m_clock = m_clock;
  assign _t9_do = tournament;
  assign _t0_p_reset = p_reset;
  assign _t0_m_clock = m_clock;
  assign _t0_do = tournament;
  assign _t3_p_reset = p_reset;
  assign _t3_m_clock = m_clock;
  assign _t3_do = tournament;
  assign _t22_p_reset = p_reset;
  assign _t22_m_clock = m_clock;
  assign _t22_do = tournament;
  assign _t31_p_reset = p_reset;
  assign _t31_m_clock = m_clock;
  assign _t31_do = tournament;
  assign _t25_p_reset = p_reset;
  assign _t25_m_clock = m_clock;
  assign _t25_do = tournament;
  assign _t19_p_reset = p_reset;
  assign _t19_m_clock = m_clock;
  assign _t19_do = tournament;
  assign _t10_p_reset = p_reset;
  assign _t10_m_clock = m_clock;
  assign _t10_do = tournament;
  assign _t13_p_reset = p_reset;
  assign _t13_m_clock = m_clock;
  assign _t13_do = tournament;
  assign _t28_p_reset = p_reset;
  assign _t28_m_clock = m_clock;
  assign _t28_do = tournament;
  assign _t16_p_reset = p_reset;
  assign _t16_m_clock = m_clock;
  assign _t16_do = tournament;
  assign _t2_p_reset = p_reset;
  assign _t2_m_clock = m_clock;
  assign _t2_do = tournament;
  assign _t5_p_reset = p_reset;
  assign _t5_m_clock = m_clock;
  assign _t5_do = tournament;
  assign _t33_p_reset = p_reset;
  assign _t33_m_clock = m_clock;
  assign _t33_do2 = __stage_COMP;
  assign _t8_p_reset = p_reset;
  assign _t8_m_clock = m_clock;
  assign _t8_do = tournament;
  assign _t15_p_reset = p_reset;
  assign _t15_m_clock = m_clock;
  assign _t15_do = tournament;
  assign _t21_p_reset = p_reset;
  assign _t21_m_clock = m_clock;
  assign _t21_do = tournament;
  assign _t24_p_reset = p_reset;
  assign _t24_m_clock = m_clock;
  assign _t24_do = tournament;
  assign _t18_p_reset = p_reset;
  assign _t18_m_clock = m_clock;
  assign _t18_do = tournament;
  assign _t30_p_reset = p_reset;
  assign _t30_m_clock = m_clock;
  assign _t30_do = tournament;
  assign _t27_p_reset = p_reset;
  assign _t27_m_clock = m_clock;
  assign _t27_do = tournament;
  assign _t12_p_reset = p_reset;
  assign _t12_m_clock = m_clock;
  assign _t12_do = tournament;
  assign _t7_p_reset = p_reset;
  assign _t7_m_clock = m_clock;
  assign _t7_do = tournament;
  /* assigns */
  assign __net1477 = select ? 1'b1 : 1'b0;
  assign _t5_in_fig1 = tournament ? fig7 : 5'bx;
  assign _t15_in_fig1 = tournament ? _t13_fig1 : 5'bx;
  assign _t4_in_val1 = tournament ? val5 : 17'bx;
  assign _t22_in_val0 = tournament ? _t20_val0 : 17'bx;
  assign _t13_in_fig0 = tournament ? fig14 : 5'bx;
  assign _t24_in_val1 = tournament ? val25 : 17'bx;
  assign _t3_in_fig0 = tournament ? _t0_fig1 : 5'bx;
  assign _t7_in_val1 = tournament ? _t5_val1 : 17'bx;
  assign _t16_in_fig0 = tournament ? fig16 : 5'bx;
  assign _t17_in_val1 = tournament ? val19 : 17'bx;
  assign _t28_in_fig0 = tournament ? fig28 : 5'bx;
  assign _t5_in_val0 = tournament ? val6 : 17'bx;
  assign _t15_in_val0 = tournament ? _t12_val1 : 17'bx;
  assign sort2 = __net1477;
  assign _t31_in_fig1 = tournament ? _t29_fig1 : 5'bx;
  assign _t24_in_fig1 = tournament ? fig25 : 5'bx;
  assign _t11_in_val1 = tournament ? _t9_val1 : 17'bx;
  assign _t32_in_fig0 = __stage_COMP ? sf0 : 5'bx;
  assign _t2_in_fig1 = tournament ? _t1_fig0 : 5'bx;
  assign _t33_in_val1 = __stage_COMP ? sv3 : 17'bx;
  assign _t0_in_fig0 = tournament ? fig0 : 5'bx;
  assign _t1_in_val1 = tournament ? val3 : 17'bx;
  assign _t10_in_fig0 = tournament ? _t8_fig0 : 5'bx;
  assign _t12_in_fig1 = tournament ? fig13 : 5'bx;
  assign _t21_in_val1 = tournament ? val23 : 17'bx;
  assign _t14_in_val1 = tournament ? _t13_val0 : 17'bx;
  assign _t25_in_fig0 = tournament ? fig26 : 5'bx;
  assign _t17_in_fig1 = tournament ? fig19 : 5'bx;
  assign _t26_in_val1 = tournament ? _t25_val0 : 17'bx;
  assign _t12_in_val0 = tournament ? val12 : 17'bx;
  assign _t7_in_fig1 = tournament ? _t5_fig1 : 5'bx;
  assign _t27_in_fig1 = tournament ? _t25_fig1 : 5'bx;
  assign _t2_in_val0 = tournament ? _t0_val0 : 17'bx;
  assign __stage_COMP_set = select;
  assign _t8_in_fig0 = tournament ? fig8 : 5'bx;
  assign _t30_in_val1 = tournament ? _t29_val0 : 17'bx;
  assign _t21_in_fig1 = tournament ? fig23 : 5'bx;
  assign _t27_in_val0 = tournament ? _t24_val1 : 17'bx;
  assign _t18_in_fig0 = tournament ? _t16_fig0 : 5'bx;
  assign out_fig = tournament ? _t2_fig0 : 5'bx;
  assign _t9_in_val1 = tournament ? val11 : 17'bx;
  assign _t19_in_val1 = tournament ? _t17_val1 : 17'bx;
  assign _t29_in_val1 = tournament ? val31 : 17'bx;
  assign _t22_in_fig0 = tournament ? _t20_fig0 : 5'bx;
  assign _t23_in_val1 = tournament ? _t21_val1 : 17'bx;
  assign _t2_in_fig0 = tournament ? _t0_fig0 : 5'bx;
  assign _t31_in_val0 = tournament ? _t28_val1 : 17'bx;
  assign _t21_in_val0 = tournament ? val22 : 17'bx;
  assign _t14_in_fig1 = tournament ? _t13_fig0 : 5'bx;
  assign _t4_in_fig1 = tournament ? fig5 : 5'bx;
  assign _t27_in_fig0 = tournament ? _t24_fig1 : 5'bx;
  assign _t29_in_fig1 = tournament ? fig31 : 5'bx;
  assign _t4_in_val0 = tournament ? val4 : 17'bx;
  assign _t14_in_val0 = tournament ? _t12_val0 : 17'bx;
  assign _t24_in_val0 = tournament ? val24 : 17'bx;
  assign _t16_in_val1 = tournament ? val17 : 17'bx;
  assign _t5_in_fig0 = tournament ? fig6 : 5'bx;
  assign _t15_in_fig0 = tournament ? _t12_fig1 : 5'bx;
  assign _t6_in_val1 = tournament ? _t5_val0 : 17'bx;
  assign __task_COMP_comp_set = select;
  assign _t7_in_val0 = tournament ? _t4_val1 : 17'bx;
  assign _t1_in_fig1 = tournament ? fig3 : 5'bx;
  assign _t20_in_val1 = tournament ? val21 : 17'bx;
  assign _t29_in_val0 = tournament ? val30 : 17'bx;
  assign _t0_in_val1 = tournament ? val1 : 17'bx;
  assign _t33_in_fig1 = __stage_COMP ? sf3 : 5'bx;
  assign _t32_in_val1 = __stage_COMP ? sv1 : 17'bx;
  assign sort1 = __net1477;
  assign _t17_in_val0 = tournament ? val18 : 17'bx;
  assign _t11_in_fig1 = tournament ? _t9_fig1 : 5'bx;
  assign _t31_in_fig0 = tournament ? _t28_fig1 : 5'bx;
  assign _t11_in_val0 = tournament ? _t8_val1 : 17'bx;
  assign _t26_in_fig1 = tournament ? _t25_fig0 : 5'bx;
  assign _t24_in_fig0 = tournament ? fig24 : 5'bx;
  assign _t1_in_val0 = tournament ? val2 : 17'bx;
  assign _t12_in_fig0 = tournament ? fig12 : 5'bx;
  assign _t33_in_val0 = __stage_COMP ? sv2 : 17'bx;
  assign _t3_in_val1 = tournament ? _t1_val1 : 17'bx;
  assign _t13_in_val1 = tournament ? val15 : 17'bx;
  assign _t19_in_fig1 = tournament ? _t17_fig1 : 5'bx;
  assign _t20_in_fig1 = tournament ? fig21 : 5'bx;
  assign _t26_in_val0 = tournament ? _t24_val0 : 17'bx;
  assign _t8_in_val1 = tournament ? val9 : 17'bx;
  assign _t17_in_fig0 = tournament ? fig18 : 5'bx;
  assign _t30_in_fig1 = tournament ? _t29_fig0 : 5'bx;
  assign _t28_in_val1 = tournament ? val29 : 17'bx;
  assign _t7_in_fig0 = tournament ? _t4_fig1 : 5'bx;
  assign _t9_in_fig1 = tournament ? fig11 : 5'bx;
  assign _t21_in_fig0 = tournament ? fig22 : 5'bx;
  assign _t9_in_val0 = tournament ? val10 : 17'bx;
  assign _t3_in_fig1 = tournament ? _t1_fig1 : 5'bx;
  assign _t22_in_val1 = tournament ? _t21_val0 : 17'bx;
  assign _t19_in_val0 = tournament ? _t16_val1 : 17'bx;
  assign _t13_in_fig1 = tournament ? fig15 : 5'bx;
  assign _t10_in_val1 = tournament ? _t9_val0 : 17'bx;
  assign _t30_in_val0 = tournament ? _t28_val0 : 17'bx;
  assign _t23_in_fig1 = tournament ? _t21_fig1 : 5'bx;
  assign __stage_COMP_reset = __stage_COMP;
  assign _t6_in_fig1 = tournament ? _t5_fig0 : 5'bx;
  assign _t15_in_val1 = tournament ? _t13_val1 : 17'bx;
  assign _t28_in_fig1 = tournament ? fig29 : 5'bx;
  assign sort3 = __net1477;
  assign _t23_in_val0 = tournament ? _t20_val1 : 17'bx;
  assign _t4_in_fig0 = tournament ? fig4 : 5'bx;
  assign _t5_in_val1 = tournament ? val7 : 17'bx;
  assign _t14_in_fig0 = tournament ? _t12_fig0 : 5'bx;
  assign _t25_in_val1 = tournament ? val27 : 17'bx;
  assign out_fig1 = __stage_COMP ? _t33_fig0 : 5'bx;
  assign _t16_in_fig1 = tournament ? fig17 : 5'bx;
  assign _t18_in_val1 = tournament ? _t17_val0 : 17'bx;
  assign _t32_in_fig1 = __stage_COMP ? sf1 : 5'bx;
  assign _t10_in_fig1 = tournament ? _t9_fig0 : 5'bx;
  assign _t16_in_val0 = tournament ? val16 : 17'bx;
  assign _t30_in_fig0 = tournament ? _t28_fig0 : 5'bx;
  assign _t0_in_fig1 = tournament ? fig1 : 5'bx;
  assign _t29_in_fig0 = tournament ? fig30 : 5'bx;
  assign _t6_in_val0 = tournament ? _t4_val0 : 17'bx;
  assign sort0 = __net1477;
  assign _t23_in_fig0 = tournament ? _t20_fig1 : 5'bx;
  assign _t33_in_fig0 = __stage_COMP ? sf2 : 5'bx;
  assign _t0_in_val0 = tournament ? val0 : 17'bx;
  assign _t25_in_fig1 = tournament ? fig27 : 5'bx;
  assign _t20_in_val0 = tournament ? val20 : 17'bx;
  assign _t1_in_fig0 = tournament ? fig2 : 5'bx;
  assign _t2_in_val1 = tournament ? _t1_val0 : 17'bx;
  assign _t11_in_fig0 = tournament ? _t8_fig1 : 5'bx;
  assign _t12_in_val1 = tournament ? val13 : 17'bx;
  assign _t32_in_val0 = __stage_COMP ? sv0 : 17'bx;
  assign _t10_in_val0 = tournament ? _t8_val0 : 17'bx;
  assign _t8_in_fig1 = tournament ? fig9 : 5'bx;
  assign _t18_in_fig1 = tournament ? _t17_fig0 : 5'bx;
  assign out_fig0 = __stage_COMP ? _t32_fig0 : 5'bx;
  assign _t6_in_fig0 = tournament ? _t4_fig0 : 5'bx;
  assign _t26_in_fig0 = tournament ? _t24_fig0 : 5'bx;
  assign _t27_in_val1 = tournament ? _t25_val1 : 17'bx;
  assign _t3_in_val0 = tournament ? _t0_val1 : 17'bx;
  assign _t13_in_val0 = tournament ? val14 : 17'bx;
  assign _t25_in_val0 = tournament ? val26 : 17'bx;
  assign _t9_in_fig0 = tournament ? fig10 : 5'bx;
  assign _t19_in_fig0 = tournament ? _t16_fig1 : 5'bx;
  assign _t31_in_val1 = tournament ? _t29_val1 : 17'bx;
  assign _t18_in_val0 = tournament ? _t16_val0 : 17'bx;
  assign _t8_in_val0 = tournament ? val8 : 17'bx;
  assign _t22_in_fig1 = tournament ? _t21_fig0 : 5'bx;
  assign _t28_in_val0 = tournament ? val28 : 17'bx;
  assign _t20_in_fig0 = tournament ? fig20 : 5'bx;
  /* regUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig12 <= 5'b0;
      else if (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1457)))))))))))))
        fig12 <= num;
      else if (tournament)
        fig12 <= _t11_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig8 <= 5'b0;
      else if (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1453)))))))))
        fig8 <= num;
      else if (tournament)
        fig8 <= _t7_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sv2 <= 17'b0;
      else if (sort2 & __net1381)
        sv2 <= val0;
      else if (sort2 & __net1382)
        sv2 <= val1;
      else if (sort2 & __net1383)
        sv2 <= val2;
      else if (sort2 & __net1384)
        sv2 <= val3;
      else if (sort2 & __net1385)
        sv2 <= val4;
      else if (sort2 & __net1386)
        sv2 <= val5;
      else if (sort2 & __net1387)
        sv2 <= val6;
      else if (sort2 & __net1388)
        sv2 <= val7;
      else if (sort2 & __net1389)
        sv2 <= val8;
      else if (sort2 & __net1390)
        sv2 <= val9;
      else if (sort2 & __net1391)
        sv2 <= val10;
      else if (sort2 & __net1392)
        sv2 <= val11;
      else if (sort2 & __net1393)
        sv2 <= val12;
      else if (sort2 & __net1394)
        sv2 <= val13;
      else if (sort2 & __net1395)
        sv2 <= val14;
      else if (sort2 & __net1396)
        sv2 <= val15;
      else if (sort2 & __net1397)
        sv2 <= val16;
      else if (sort2 & __net1398)
        sv2 <= val17;
      else if (sort2 & __net1399)
        sv2 <= val18;
      else if (sort2 & __net1400)
        sv2 <= val19;
      else if (sort2 & __net1401)
        sv2 <= val20;
      else if (sort2 & __net1402)
        sv2 <= val21;
      else if (sort2 & __net1403)
        sv2 <= val22;
      else if (sort2 & __net1404)
        sv2 <= val23;
      else if (sort2 & __net1405)
        sv2 <= val24;
      else if (sort2 & __net1406)
        sv2 <= val25;
      else if (sort2 & __net1407)
        sv2 <= val26;
      else if (sort2 & __net1408)
        sv2 <= val27;
      else if (sort2 & __net1409)
        sv2 <= val28;
      else if (sort2 & __net1410)
        sv2 <= val29;
      else if (sort2 & __net1411)
        sv2 <= val30;
      else if (sort2 & __net1412)
        sv2 <= val31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig2 <= 5'b0;
      else if (~(write & __net1446) & (~(write & __net1445) & (write & __net1447)))
        fig2 <= num;
      else if (tournament)
        fig2 <= _t3_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val22 <= 17'b0;
      else if (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1467)))))))))))))))))))))))
        val22 <= in_val;
      else if (tournament)
        val22 <= _t23_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val1 <= 17'b0;
      else if (~(write & __net1445) & (write & __net1446))
        val1 <= in_val;
      else if (tournament)
        val1 <= _t2_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig23 <= 5'b0;
      else if (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1468))))))))))))))))))))))))
        fig23 <= num;
      else if (tournament)
        fig23 <= _t26_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val10 <= 17'b0;
      else if (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1455)))))))))))
        val10 <= in_val;
      else if (tournament)
        val10 <= _t11_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val19 <= 17'b0;
      else if (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1464))))))))))))))))))))
        val19 <= in_val;
      else if (tournament)
        val19 <= _t22_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sf1 <= 5'b0;
      else if (sort1 & __net1349)
        sf1 <= fig0;
      else if (sort1 & __net1350)
        sf1 <= fig1;
      else if (sort1 & __net1351)
        sf1 <= fig2;
      else if (sort1 & __net1352)
        sf1 <= fig3;
      else if (sort1 & __net1353)
        sf1 <= fig4;
      else if (sort1 & __net1354)
        sf1 <= fig5;
      else if (sort1 & __net1355)
        sf1 <= fig6;
      else if (sort1 & __net1356)
        sf1 <= fig7;
      else if (sort1 & __net1357)
        sf1 <= fig8;
      else if (sort1 & __net1358)
        sf1 <= fig9;
      else if (sort1 & __net1359)
        sf1 <= fig10;
      else if (sort1 & __net1360)
        sf1 <= fig11;
      else if (sort1 & __net1361)
        sf1 <= fig12;
      else if (sort1 & __net1362)
        sf1 <= fig13;
      else if (sort1 & __net1363)
        sf1 <= fig14;
      else if (sort1 & __net1364)
        sf1 <= fig15;
      else if (sort1 & __net1365)
        sf1 <= fig16;
      else if (sort1 & __net1366)
        sf1 <= fig17;
      else if (sort1 & __net1367)
        sf1 <= fig18;
      else if (sort1 & __net1368)
        sf1 <= fig19;
      else if (sort1 & __net1369)
        sf1 <= fig20;
      else if (sort1 & __net1370)
        sf1 <= fig21;
      else if (sort1 & __net1371)
        sf1 <= fig22;
      else if (sort1 & __net1372)
        sf1 <= fig23;
      else if (sort1 & __net1373)
        sf1 <= fig24;
      else if (sort1 & __net1374)
        sf1 <= fig25;
      else if (sort1 & __net1375)
        sf1 <= fig26;
      else if (sort1 & __net1376)
        sf1 <= fig27;
      else if (sort1 & __net1377)
        sf1 <= fig28;
      else if (sort1 & __net1378)
        sf1 <= fig29;
      else if (sort1 & __net1379)
        sf1 <= fig30;
      else if (sort1 & __net1380)
        sf1 <= fig31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val25 <= 17'b0;
      else if (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1470))))))))))))))))))))))))))
        val25 <= in_val;
      else if (tournament)
        val25 <= _t26_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig5 <= 5'b0;
      else if (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1450))))))
        fig5 <= num;
      else if (tournament)
        fig5 <= _t6_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val31 <= 17'b0;
      else if (~(write & __net1475) & (~(write & __net1474) & (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1476))))))))))))))))))))))))))))))))
        val31 <= in_val;
      else if (tournament)
        val31 <= _t31_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig26 <= 5'b0;
      else if (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1471)))))))))))))))))))))))))))
        fig26 <= num;
      else if (tournament)
        fig26 <= _t27_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig11 <= 5'b0;
      else if (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1456))))))))))))
        fig11 <= num;
      else if (tournament)
        fig11 <= _t14_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val13 <= 17'b0;
      else if (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1458))))))))))))))
        val13 <= in_val;
      else if (tournament)
        val13 <= _t14_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val28 <= 17'b0;
      else if (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1473)))))))))))))))))))))))))))))
        val28 <= in_val;
      else if (tournament)
        val28 <= _t27_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val4 <= 17'b0;
      else if (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1449)))))
        val4 <= in_val;
      else if (tournament)
        val4 <= _t3_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig20 <= 5'b0;
      else if (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1465)))))))))))))))))))))
        fig20 <= num;
      else if (tournament)
        fig20 <= _t19_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val16 <= 17'b0;
      else if (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1461)))))))))))))))))
        val16 <= in_val;
      else if (tournament)
        val16 <= _t15_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val7 <= 17'b0;
      else if (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1452))))))))
        val7 <= in_val;
      else if (tournament)
        val7 <= _t10_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig14 <= 5'b0;
      else if (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1459)))))))))))))))
        fig14 <= num;
      else if (tournament)
        fig14 <= _t15_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig29 <= 5'b0;
      else if (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1474))))))))))))))))))))))))))))))
        fig29 <= num;
      else if (tournament)
        fig29 <= _t30_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig17 <= 5'b0;
      else if (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1462))))))))))))))))))
        fig17 <= num;
      else if (tournament)
        fig17 <= _t18_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig7 <= 5'b0;
      else if (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1452))))))))
        fig7 <= num;
      else if (tournament)
        fig7 <= _t10_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sv1 <= 17'b0;
      else if (sort1 & __net1349)
        sv1 <= val0;
      else if (sort1 & __net1350)
        sv1 <= val1;
      else if (sort1 & __net1351)
        sv1 <= val2;
      else if (sort1 & __net1352)
        sv1 <= val3;
      else if (sort1 & __net1353)
        sv1 <= val4;
      else if (sort1 & __net1354)
        sv1 <= val5;
      else if (sort1 & __net1355)
        sv1 <= val6;
      else if (sort1 & __net1356)
        sv1 <= val7;
      else if (sort1 & __net1357)
        sv1 <= val8;
      else if (sort1 & __net1358)
        sv1 <= val9;
      else if (sort1 & __net1359)
        sv1 <= val10;
      else if (sort1 & __net1360)
        sv1 <= val11;
      else if (sort1 & __net1361)
        sv1 <= val12;
      else if (sort1 & __net1362)
        sv1 <= val13;
      else if (sort1 & __net1363)
        sv1 <= val14;
      else if (sort1 & __net1364)
        sv1 <= val15;
      else if (sort1 & __net1365)
        sv1 <= val16;
      else if (sort1 & __net1366)
        sv1 <= val17;
      else if (sort1 & __net1367)
        sv1 <= val18;
      else if (sort1 & __net1368)
        sv1 <= val19;
      else if (sort1 & __net1369)
        sv1 <= val20;
      else if (sort1 & __net1370)
        sv1 <= val21;
      else if (sort1 & __net1371)
        sv1 <= val22;
      else if (sort1 & __net1372)
        sv1 <= val23;
      else if (sort1 & __net1373)
        sv1 <= val24;
      else if (sort1 & __net1374)
        sv1 <= val25;
      else if (sort1 & __net1375)
        sv1 <= val26;
      else if (sort1 & __net1376)
        sv1 <= val27;
      else if (sort1 & __net1377)
        sv1 <= val28;
      else if (sort1 & __net1378)
        sv1 <= val29;
      else if (sort1 & __net1379)
        sv1 <= val30;
      else if (sort1 & __net1380)
        sv1 <= val31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig1 <= 5'b0;
      else if (~(write & __net1445) & (write & __net1446))
        fig1 <= num;
      else if (tournament)
        fig1 <= _t2_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val6 <= 17'b0;
      else if (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1451)))))))
        val6 <= in_val;
      else if (tournament)
        val6 <= _t7_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val21 <= 17'b0;
      else if (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1466))))))))))))))))))))))
        val21 <= in_val;
      else if (tournament)
        val21 <= _t22_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val15 <= 17'b0;
      else if (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1460))))))))))))))))
        val15 <= in_val;
      else if (tournament)
        val15 <= _t18_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig22 <= 5'b0;
      else if (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1467)))))))))))))))))))))))
        fig22 <= num;
      else if (tournament)
        fig22 <= _t23_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val18 <= 17'b0;
      else if (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1463)))))))))))))))))))
        val18 <= in_val;
      else if (tournament)
        val18 <= _t19_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig16 <= 5'b0;
      else if (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1461)))))))))))))))))
        fig16 <= num;
      else if (tournament)
        fig16 <= _t15_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val0 <= 17'b0;
      else if (write & __net1445)
        val0 <= in_val;
      else if (tournament)
        val0 <= in_val;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val9 <= 17'b0;
      else if (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1454))))))))))
        val9 <= in_val;
      else if (tournament)
        val9 <= _t10_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val24 <= 17'b0;
      else if (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1469)))))))))))))))))))))))))
        val24 <= in_val;
      else if (tournament)
        val24 <= _t23_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sf0 <= 5'b0;
      else if (sort0 & __net1317)
        sf0 <= fig0;
      else if (sort0 & __net1318)
        sf0 <= fig1;
      else if (sort0 & __net1319)
        sf0 <= fig2;
      else if (sort0 & __net1320)
        sf0 <= fig3;
      else if (sort0 & __net1321)
        sf0 <= fig4;
      else if (sort0 & __net1322)
        sf0 <= fig5;
      else if (sort0 & __net1323)
        sf0 <= fig6;
      else if (sort0 & __net1324)
        sf0 <= fig7;
      else if (sort0 & __net1325)
        sf0 <= fig8;
      else if (sort0 & __net1326)
        sf0 <= fig9;
      else if (sort0 & __net1327)
        sf0 <= fig10;
      else if (sort0 & __net1328)
        sf0 <= fig11;
      else if (sort0 & __net1329)
        sf0 <= fig12;
      else if (sort0 & __net1330)
        sf0 <= fig13;
      else if (sort0 & __net1331)
        sf0 <= fig14;
      else if (sort0 & __net1332)
        sf0 <= fig15;
      else if (sort0 & __net1333)
        sf0 <= fig16;
      else if (sort0 & __net1334)
        sf0 <= fig17;
      else if (sort0 & __net1335)
        sf0 <= fig18;
      else if (sort0 & __net1336)
        sf0 <= fig19;
      else if (sort0 & __net1337)
        sf0 <= fig20;
      else if (sort0 & __net1338)
        sf0 <= fig21;
      else if (sort0 & __net1339)
        sf0 <= fig22;
      else if (sort0 & __net1340)
        sf0 <= fig23;
      else if (sort0 & __net1341)
        sf0 <= fig24;
      else if (sort0 & __net1342)
        sf0 <= fig25;
      else if (sort0 & __net1343)
        sf0 <= fig26;
      else if (sort0 & __net1344)
        sf0 <= fig27;
      else if (sort0 & __net1345)
        sf0 <= fig28;
      else if (sort0 & __net1346)
        sf0 <= fig29;
      else if (sort0 & __net1347)
        sf0 <= fig30;
      else if (sort0 & __net1348)
        sf0 <= fig31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val30 <= 17'b0;
      else if (~(write & __net1474) & (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1475)))))))))))))))))))))))))))))))
        val30 <= in_val;
      else if (tournament)
        val30 <= _t31_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig4 <= 5'b0;
      else if (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1449)))))
        fig4 <= num;
      else if (tournament)
        fig4 <= _t3_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig31 <= 5'b0;
      else if (~(write & __net1475) & (~(write & __net1474) & (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1476))))))))))))))))))))))))))))))))
        fig31 <= num;
      else if (tournament)
        fig31 <= _t31_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig25 <= 5'b0;
      else if (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1470))))))))))))))))))))))))))
        fig25 <= num;
      else if (tournament)
        fig25 <= _t26_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val12 <= 17'b0;
      else if (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1457)))))))))))))
        val12 <= in_val;
      else if (tournament)
        val12 <= _t11_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val27 <= 17'b0;
      else if (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1472))))))))))))))))))))))))))))
        val27 <= in_val;
      else if (tournament)
        val27 <= _t30_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sf3 <= 5'b0;
      else if (sort3 & __net1413)
        sf3 <= fig0;
      else if (sort3 & __net1414)
        sf3 <= fig1;
      else if (sort3 & __net1415)
        sf3 <= fig2;
      else if (sort3 & __net1416)
        sf3 <= fig3;
      else if (sort3 & __net1417)
        sf3 <= fig4;
      else if (sort3 & __net1418)
        sf3 <= fig5;
      else if (sort3 & __net1419)
        sf3 <= fig6;
      else if (sort3 & __net1420)
        sf3 <= fig7;
      else if (sort3 & __net1421)
        sf3 <= fig8;
      else if (sort3 & __net1422)
        sf3 <= fig9;
      else if (sort3 & __net1423)
        sf3 <= fig10;
      else if (sort3 & __net1424)
        sf3 <= fig11;
      else if (sort3 & __net1425)
        sf3 <= fig12;
      else if (sort3 & __net1426)
        sf3 <= fig13;
      else if (sort3 & __net1427)
        sf3 <= fig14;
      else if (sort3 & __net1428)
        sf3 <= fig15;
      else if (sort3 & __net1429)
        sf3 <= fig16;
      else if (sort3 & __net1430)
        sf3 <= fig17;
      else if (sort3 & __net1431)
        sf3 <= fig18;
      else if (sort3 & __net1432)
        sf3 <= fig19;
      else if (sort3 & __net1433)
        sf3 <= fig20;
      else if (sort3 & __net1434)
        sf3 <= fig21;
      else if (sort3 & __net1435)
        sf3 <= fig22;
      else if (sort3 & __net1436)
        sf3 <= fig23;
      else if (sort3 & __net1437)
        sf3 <= fig24;
      else if (sort3 & __net1438)
        sf3 <= fig25;
      else if (sort3 & __net1439)
        sf3 <= fig26;
      else if (sort3 & __net1440)
        sf3 <= fig27;
      else if (sort3 & __net1441)
        sf3 <= fig28;
      else if (sort3 & __net1442)
        sf3 <= fig29;
      else if (sort3 & __net1443)
        sf3 <= fig30;
      else if (sort3 & __net1444)
        sf3 <= fig31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig19 <= 5'b0;
      else if (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1464))))))))))))))))))))
        fig19 <= num;
      else if (tournament)
        fig19 <= _t22_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig10 <= 5'b0;
      else if (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1455)))))))))))
        fig10 <= num;
      else if (tournament)
        fig10 <= _t11_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val3 <= 17'b0;
      else if (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1448))))
        val3 <= in_val;
      else if (tournament)
        val3 <= _t6_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sv0 <= 17'b0;
      else if (sort0 & __net1317)
        sv0 <= val0;
      else if (sort0 & __net1318)
        sv0 <= val1;
      else if (sort0 & __net1319)
        sv0 <= val2;
      else if (sort0 & __net1320)
        sv0 <= val3;
      else if (sort0 & __net1321)
        sv0 <= val4;
      else if (sort0 & __net1322)
        sv0 <= val5;
      else if (sort0 & __net1323)
        sv0 <= val6;
      else if (sort0 & __net1324)
        sv0 <= val7;
      else if (sort0 & __net1325)
        sv0 <= val8;
      else if (sort0 & __net1326)
        sv0 <= val9;
      else if (sort0 & __net1327)
        sv0 <= val10;
      else if (sort0 & __net1328)
        sv0 <= val11;
      else if (sort0 & __net1329)
        sv0 <= val12;
      else if (sort0 & __net1330)
        sv0 <= val13;
      else if (sort0 & __net1331)
        sv0 <= val14;
      else if (sort0 & __net1332)
        sv0 <= val15;
      else if (sort0 & __net1333)
        sv0 <= val16;
      else if (sort0 & __net1334)
        sv0 <= val17;
      else if (sort0 & __net1335)
        sv0 <= val18;
      else if (sort0 & __net1336)
        sv0 <= val19;
      else if (sort0 & __net1337)
        sv0 <= val20;
      else if (sort0 & __net1338)
        sv0 <= val21;
      else if (sort0 & __net1339)
        sv0 <= val22;
      else if (sort0 & __net1340)
        sv0 <= val23;
      else if (sort0 & __net1341)
        sv0 <= val24;
      else if (sort0 & __net1342)
        sv0 <= val25;
      else if (sort0 & __net1343)
        sv0 <= val26;
      else if (sort0 & __net1344)
        sv0 <= val27;
      else if (sort0 & __net1345)
        sv0 <= val28;
      else if (sort0 & __net1346)
        sv0 <= val29;
      else if (sort0 & __net1347)
        sv0 <= val30;
      else if (sort0 & __net1348)
        sv0 <= val31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig28 <= 5'b0;
      else if (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1473)))))))))))))))))))))))))))))
        fig28 <= num;
      else if (tournament)
        fig28 <= _t27_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sv3 <= 17'b0;
      else if (sort3 & __net1413)
        sv3 <= val0;
      else if (sort3 & __net1414)
        sv3 <= val1;
      else if (sort3 & __net1415)
        sv3 <= val2;
      else if (sort3 & __net1416)
        sv3 <= val3;
      else if (sort3 & __net1417)
        sv3 <= val4;
      else if (sort3 & __net1418)
        sv3 <= val5;
      else if (sort3 & __net1419)
        sv3 <= val6;
      else if (sort3 & __net1420)
        sv3 <= val7;
      else if (sort3 & __net1421)
        sv3 <= val8;
      else if (sort3 & __net1422)
        sv3 <= val9;
      else if (sort3 & __net1423)
        sv3 <= val10;
      else if (sort3 & __net1424)
        sv3 <= val11;
      else if (sort3 & __net1425)
        sv3 <= val12;
      else if (sort3 & __net1426)
        sv3 <= val13;
      else if (sort3 & __net1427)
        sv3 <= val14;
      else if (sort3 & __net1428)
        sv3 <= val15;
      else if (sort3 & __net1429)
        sv3 <= val16;
      else if (sort3 & __net1430)
        sv3 <= val17;
      else if (sort3 & __net1431)
        sv3 <= val18;
      else if (sort3 & __net1432)
        sv3 <= val19;
      else if (sort3 & __net1433)
        sv3 <= val20;
      else if (sort3 & __net1434)
        sv3 <= val21;
      else if (sort3 & __net1435)
        sv3 <= val22;
      else if (sort3 & __net1436)
        sv3 <= val23;
      else if (sort3 & __net1437)
        sv3 <= val24;
      else if (sort3 & __net1438)
        sv3 <= val25;
      else if (sort3 & __net1439)
        sv3 <= val26;
      else if (sort3 & __net1440)
        sv3 <= val27;
      else if (sort3 & __net1441)
        sv3 <= val28;
      else if (sort3 & __net1442)
        sv3 <= val29;
      else if (sort3 & __net1443)
        sv3 <= val30;
      else if (sort3 & __net1444)
        sv3 <= val31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig13 <= 5'b0;
      else if (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1458))))))))))))))
        fig13 <= num;
      else if (tournament)
        fig13 <= _t14_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig3 <= 5'b0;
      else if (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1448))))
        fig3 <= num;
      else if (tournament)
        fig3 <= _t6_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig6 <= 5'b0;
      else if (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1451)))))))
        fig6 <= num;
      else if (tournament)
        fig6 <= _t7_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val11 <= 17'b0;
      else if (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1456))))))))))))
        val11 <= in_val;
      else if (tournament)
        val11 <= _t14_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        sf2 <= 5'b0;
      else if (sort2 & __net1381)
        sf2 <= fig0;
      else if (sort2 & __net1382)
        sf2 <= fig1;
      else if (sort2 & __net1383)
        sf2 <= fig2;
      else if (sort2 & __net1384)
        sf2 <= fig3;
      else if (sort2 & __net1385)
        sf2 <= fig4;
      else if (sort2 & __net1386)
        sf2 <= fig5;
      else if (sort2 & __net1387)
        sf2 <= fig6;
      else if (sort2 & __net1388)
        sf2 <= fig7;
      else if (sort2 & __net1389)
        sf2 <= fig8;
      else if (sort2 & __net1390)
        sf2 <= fig9;
      else if (sort2 & __net1391)
        sf2 <= fig10;
      else if (sort2 & __net1392)
        sf2 <= fig11;
      else if (sort2 & __net1393)
        sf2 <= fig12;
      else if (sort2 & __net1394)
        sf2 <= fig13;
      else if (sort2 & __net1395)
        sf2 <= fig14;
      else if (sort2 & __net1396)
        sf2 <= fig15;
      else if (sort2 & __net1397)
        sf2 <= fig16;
      else if (sort2 & __net1398)
        sf2 <= fig17;
      else if (sort2 & __net1399)
        sf2 <= fig18;
      else if (sort2 & __net1400)
        sf2 <= fig19;
      else if (sort2 & __net1401)
        sf2 <= fig20;
      else if (sort2 & __net1402)
        sf2 <= fig21;
      else if (sort2 & __net1403)
        sf2 <= fig22;
      else if (sort2 & __net1404)
        sf2 <= fig23;
      else if (sort2 & __net1405)
        sf2 <= fig24;
      else if (sort2 & __net1406)
        sf2 <= fig25;
      else if (sort2 & __net1407)
        sf2 <= fig26;
      else if (sort2 & __net1408)
        sf2 <= fig27;
      else if (sort2 & __net1409)
        sf2 <= fig28;
      else if (sort2 & __net1410)
        sf2 <= fig29;
      else if (sort2 & __net1411)
        sf2 <= fig30;
      else if (sort2 & __net1412)
        sf2 <= fig31;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val26 <= 17'b0;
      else if (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1471)))))))))))))))))))))))))))
        val26 <= in_val;
      else if (tournament)
        val26 <= _t27_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig27 <= 5'b0;
      else if (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1472))))))))))))))))))))))))))))
        fig27 <= num;
      else if (tournament)
        fig27 <= _t30_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val5 <= 17'b0;
      else if (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1450))))))
        val5 <= in_val;
      else if (tournament)
        val5 <= _t6_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val29 <= 17'b0;
      else if (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1474))))))))))))))))))))))))))))))
        val29 <= in_val;
      else if (tournament)
        val29 <= _t30_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val14 <= 17'b0;
      else if (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1459)))))))))))))))
        val14 <= in_val;
      else if (tournament)
        val14 <= _t15_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig0 <= 5'b0;
      else if (write & __net1445)
        fig0 <= num;
      else if (tournament)
        fig0 <= _t2_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig9 <= 5'b0;
      else if (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1454))))))))))
        fig9 <= num;
      else if (tournament)
        fig9 <= _t10_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val20 <= 17'b0;
      else if (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1465)))))))))))))))))))))
        val20 <= in_val;
      else if (tournament)
        val20 <= _t19_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val23 <= 17'b0;
      else if (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1468))))))))))))))))))))))))
        val23 <= in_val;
      else if (tournament)
        val23 <= _t26_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val17 <= 17'b0;
      else if (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1462))))))))))))))))))
        val17 <= in_val;
      else if (tournament)
        val17 <= _t18_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig21 <= 5'b0;
      else if (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1466))))))))))))))))))))))
        fig21 <= num;
      else if (tournament)
        fig21 <= _t22_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig15 <= 5'b0;
      else if (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1460))))))))))))))))
        fig15 <= num;
      else if (tournament)
        fig15 <= _t18_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val8 <= 17'b0;
      else if (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1453)))))))))
        val8 <= in_val;
      else if (tournament)
        val8 <= _t7_val1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val2 <= 17'b0;
      else if (~(write & __net1446) & (~(write & __net1445) & (write & __net1447)))
        val2 <= in_val;
      else if (tournament)
        val2 <= _t3_val0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig30 <= 5'b0;
      else if (~(write & __net1474) & (~(write & __net1473) & (~(write & __net1472) & (~(write & __net1471) & (~(write & __net1470) & (~(write & __net1469) & (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1475)))))))))))))))))))))))))))))))
        fig30 <= num;
      else if (tournament)
        fig30 <= _t31_fig0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig24 <= 5'b0;
      else if (~(write & __net1468) & (~(write & __net1467) & (~(write & __net1466) & (~(write & __net1465) & (~(write & __net1464) & (~(write & __net1463) & (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1469)))))))))))))))))))))))))
        fig24 <= num;
      else if (tournament)
        fig24 <= _t23_fig1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        fig18 <= 5'b0;
      else if (~(write & __net1462) & (~(write & __net1461) & (~(write & __net1460) & (~(write & __net1459) & (~(write & __net1458) & (~(write & __net1457) & (~(write & __net1456) & (~(write & __net1455) & (~(write & __net1454) & (~(write & __net1453) & (~(write & __net1452) & (~(write & __net1451) & (~(write & __net1450) & (~(write & __net1449) & (~(write & __net1448) & (~(write & __net1447) & (~(write & __net1446) & (~(write & __net1445) & (write & __net1463)))))))))))))))))))
        fig18 <= num;
      else if (tournament)
        fig18 <= _t19_fig0;
    end
  /* stageRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_COMP <= 1'b0;
      else if (__stage_COMP_set | __stage_COMP_reset)
        __stage_COMP <= __stage_COMP_set;
    end
  /* taskRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_COMP_comp <= 1'b0;
      else if (__task_COMP_comp_set | __stage_COMP_reset)
        __task_COMP_comp <= __task_COMP_comp_set;
    end
endmodule
/* End of file (gen.v) */
