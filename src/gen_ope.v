/* declare fitness256 */
/* declare mutate256 */
/* declare cross */
/* declare init */

module gen_ope(p_reset, m_clock, M_INIT, FIT_INIT, FIRST, CROSS, fig, rand, gen0, gen1, num, max_vol, in_val, in_vol, out, gen, val);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input M_INIT;
  wire M_INIT;
  input FIT_INIT;
  wire FIT_INIT;
  input FIRST;
  wire FIRST;
  input CROSS;
  wire CROSS;
  input [31:0] fig;
  wire [31:0] fig;
  input [7:0] rand;
  wire [7:0] rand;
  input [255:0] gen0;
  wire [255:0] gen0;
  input [255:0] gen1;
  wire [255:0] gen1;
  input [2:0] num;
  wire [2:0] num;
  input [15:0] max_vol;
  wire [15:0] max_vol;
  input [255:0] in_val;
  wire [255:0] in_val;
  input [255:0] in_vol;
  wire [255:0] in_vol;
  output [31:0] out;
  wire [31:0] out;
  output [255:0] gen;
  wire [255:0] gen;
  output [16:0] val;
  wire [16:0] val;
  /* elements */
  reg [255:0] reg_gen0;
  reg [255:0] reg_gen1;
  reg [255:0] reg_gen2;
  reg [255:0] reg_gen3;
  /* reg decls */
  reg __stage_MUTATE;
  reg __stage_FITNESS0;
  reg __stage_FITNESS1;
  reg __stage_FITNESS2;
  reg __task_MUTATE_mutate;
  reg __task_FITNESS0_fit0;
  reg __task_FITNESS1_fit1;
  reg __task_FITNESS2_fit2;
  /* net decls */
  wire __net1481;
  wire __stage_MUTATE_reset;
  wire __stage_MUTATE_set;
  wire __stage_FITNESS0_reset;
  wire __stage_FITNESS0_set;
  wire __stage_FITNESS1_reset;
  wire __stage_FITNESS1_set;
  wire __stage_FITNESS2_reset;
  wire __stage_FITNESS2_set;
  wire __task_MUTATE_mutate_set;
  wire __task_FITNESS0_fit0_set;
  wire __task_FITNESS1_fit1_set;
  wire __task_FITNESS2_fit2_set;
  wire [255:0] __net1480;
  wire [255:0] __net1479;
  wire [255:0] __net1478;
  /* sub-modules */
  wire [31:0] _init_fig;
  wire [255:0] _init_out_gen;
  wire [31:0] _init_out;
  wire _init_do;
  wire _init_p_reset;
  wire _init_m_clock;
  wire [7:0] _cross_randd;
  wire [255:0] _cross_gen0;
  wire [255:0] _cross_gen1;
  wire [255:0] _cross_out;
  wire _cross_do;
  wire _cross_p_reset;
  wire _cross_m_clock;
  wire [255:0] _mutate_in;
  wire [31:0] _mutate_out_fig;
  wire [255:0] _mutate_out;
  wire _mutate_init;
  wire _mutate_do;
  wire _mutate_p_reset;
  wire _mutate_m_clock;
  wire [255:0] _fitness_in;
  wire [2:0] _fitness_num;
  wire [15:0] _fitness_max_vol;
  wire [255:0] _fitness_in_val;
  wire [255:0] _fitness_in_vol;
  wire [16:0] _fitness_val;
  wire _fitness_init;
  wire _fitness_do0;
  wire _fitness_do1;
  wire _fitness_do2;
  wire _fitness_p_reset;
  wire _fitness_m_clock;
  init init(.p_reset(_init_p_reset),
            .m_clock(_init_m_clock),
            .fig(_init_fig),
            .out_gen(_init_out_gen),
            .out(_init_out),
            .do(_init_do));
  cross cross(.p_reset(_cross_p_reset),
              .m_clock(_cross_m_clock),
              .randd(_cross_randd),
              .gen0(_cross_gen0),
              .gen1(_cross_gen1),
              .out(_cross_out),
              .do(_cross_do));
  mutate256 mutate(.p_reset(_mutate_p_reset),
                   .m_clock(_mutate_m_clock),
                   .in(_mutate_in),
                   .out_fig(_mutate_out_fig),
                   .out(_mutate_out),
                   .init(_mutate_init),
                   .do(_mutate_do));
  fitness256 fitness(.p_reset(_fitness_p_reset),
                     .m_clock(_fitness_m_clock),
                     .in(_fitness_in),
                     .num(_fitness_num),
                     .max_vol(_fitness_max_vol),
                     .in_val(_fitness_in_val),
                     .in_vol(_fitness_in_vol),
                     .val(_fitness_val),
                     .init(_fitness_init),
                     .do0(_fitness_do0),
                     .do1(_fitness_do1),
                     .do2(_fitness_do2));
  /* invokes */
  assign _cross_p_reset = p_reset;
  assign _cross_m_clock = m_clock;
  assign _cross_do = CROSS;
  assign _init_p_reset = p_reset;
  assign _init_m_clock = m_clock;
  assign _init_do = FIRST;
  assign _mutate_p_reset = p_reset;
  assign _mutate_m_clock = m_clock;
  assign _mutate_init = M_INIT;
  assign _mutate_do = __stage_MUTATE;
  assign _fitness_p_reset = p_reset;
  assign _fitness_m_clock = m_clock;
  assign _fitness_do1 = __stage_FITNESS1;
  assign _fitness_init = FIT_INIT;
  assign _fitness_do0 = __stage_FITNESS0;
  assign _fitness_do2 = __stage_FITNESS2;
  /* assigns */
  assign __net1481 = __stage_MUTATE | FIRST;
  assign _cross_gen1 = CROSS ? gen1 : 256'bx;
  assign __stage_MUTATE_set = CROSS;
  assign __stage_FITNESS0_set = __net1481;
  assign __stage_FITNESS1_reset = __stage_FITNESS1;
  assign gen = __stage_FITNESS2 ? reg_gen3 : 256'bx;
  assign __task_FITNESS0_fit0_set = __net1481;
  assign __stage_FITNESS1_set = __stage_FITNESS0;
  assign __net1478 = FIRST ? _init_out_gen : 256'bx;
  assign _init_fig = FIRST ? fig : 32'bx;
  assign __stage_FITNESS0_reset = __stage_FITNESS0;
  assign _fitness_in = __stage_FITNESS0 ? reg_gen1 : 256'bx;
  assign val = __stage_FITNESS2 ? _fitness_val : 17'bx;
  assign __task_FITNESS1_fit1_set = __stage_FITNESS0;
  assign __stage_MUTATE_reset = __stage_MUTATE;
  assign _mutate_in = __stage_MUTATE ? reg_gen0 : 256'bx;
  assign _fitness_in_vol = FIT_INIT ? in_vol : 256'bx;
  assign __task_FITNESS2_fit2_set = __stage_FITNESS1;
  assign _cross_gen0 = CROSS ? gen0 : 256'bx;
  assign _cross_randd = CROSS ? rand : 8'bx;
  assign __stage_FITNESS2_set = __stage_FITNESS1;
  assign __net1480 = __stage_MUTATE ? _mutate_out : 256'bx;
  assign _fitness_in_val = FIT_INIT ? in_val : 256'bx;
  assign __task_MUTATE_mutate_set = CROSS;
  assign _fitness_num = FIT_INIT ? num : 3'bx;
  assign out = FIRST ? _init_out : 32'bx;
  assign __stage_FITNESS2_reset = __stage_FITNESS2;
  assign __net1479 = CROSS ? _cross_out : 256'bx;
  assign _fitness_max_vol = FIT_INIT ? max_vol : 16'bx;
  /* regUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_gen2 <= 256'b0;
      else if (__stage_FITNESS0)
        reg_gen2 <= reg_gen1;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_gen1 <= 256'b0;
      else if (FIRST)
        reg_gen1 <= __net1478;
      else if (__stage_MUTATE)
        reg_gen1 <= __net1480;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_gen0 <= 256'b0;
      else if (CROSS)
        reg_gen0 <= __net1479;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_gen3 <= 256'b0;
      else if (__stage_FITNESS1)
        reg_gen3 <= reg_gen2;
    end
  /* stageRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_FITNESS2 <= 1'b0;
      else if (__stage_FITNESS2_set | __stage_FITNESS2_reset)
        __stage_FITNESS2 <= __stage_FITNESS2_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_FITNESS1 <= 1'b0;
      else if (__stage_FITNESS1_set | __stage_FITNESS1_reset)
        __stage_FITNESS1 <= __stage_FITNESS1_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_FITNESS0 <= 1'b0;
      else if (__stage_FITNESS0_set | __stage_FITNESS0_reset)
        __stage_FITNESS0 <= __stage_FITNESS0_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_MUTATE <= 1'b0;
      else if (__stage_MUTATE_set | __stage_MUTATE_reset)
        __stage_MUTATE <= __stage_MUTATE_set;
    end
  /* taskRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_FITNESS2_fit2 <= 1'b0;
      else if (__task_FITNESS2_fit2_set | __stage_FITNESS2_reset)
        __task_FITNESS2_fit2 <= __task_FITNESS2_fit2_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_FITNESS1_fit1 <= 1'b0;
      else if (__task_FITNESS1_fit1_set | __stage_FITNESS1_reset)
        __task_FITNESS1_fit1 <= __task_FITNESS1_fit1_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_FITNESS0_fit0 <= 1'b0;
      else if (__task_FITNESS0_fit0_set | __stage_FITNESS0_reset)
        __task_FITNESS0_fit0 <= __task_FITNESS0_fit0_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_MUTATE_mutate <= 1'b0;
      else if (__task_MUTATE_mutate_set | __stage_MUTATE_reset)
        __task_MUTATE_mutate <= __task_MUTATE_mutate_set;
    end
endmodule
/* End of file (gen_ope.v) */
