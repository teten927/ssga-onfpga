/* declare memunit */
/* declare rand */
/* declare gen_ope */
/* declare gen */
/* declare link */

module top(p_reset, m_clock, do, in_val, in_vol, max_vol, gen_out, val);
  parameter __state_CULC__init0 = 0;
  parameter __state_CULC__init1 = 1;
  parameter __state_CULC__init_loop = 2;
  parameter __state_CULC__loop = 3;
  parameter __state_FIRST__INPUT = 0;
  parameter __state_FIRST__INIT0 = 1;
  parameter __state_FIRST__INIT1 = 2;
  parameter __state_FIRST__INIT2 = 3;
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [7:0] in_val;
  wire [7:0] in_val;
  input [7:0] in_vol;
  wire [7:0] in_vol;
  input [15:0] max_vol;
  wire [15:0] max_vol;
  output [255:0] gen_out;
  wire [255:0] gen_out;
  output [16:0] val;
  wire [16:0] val;
  /* elements */
  reg [15:0] MAX_VOL;
  reg [255:0] reg_out0;
  reg [255:0] reg_out1;
  reg [255:0] save_gen;
  reg [16:0] save_val;
  reg [31:0] save_fig;
  reg [4:0] count;
  reg [4:0] count_;
  reg [2:0] count8;
  /* reg decls */
  reg __stage_FIRST;
  reg __stage_INIT;
  reg __stage_CULC;
  reg __task_CULC_culc;
  reg __task_INIT_init;
  reg __task_FIRST_first;
  reg [1:0] __stage_CULC_state_reg;
  reg [1:0] __stage_FIRST_state_reg;
  /* net decls */
  wire __net2092;
  wire __net2091;
  wire __net2090;
  wire __net2089;
  wire __net2088;
  wire __net2087;
  wire __net2086;
  wire __net2085;
  wire __net2084;
  wire __net2083;
  wire __net2082;
  wire __net2081;
  wire __net2080;
  wire __net2079;
  wire __net2078;
  wire __net2077;
  wire __net2076;
  wire __net2075;
  wire __net2074;
  wire __net2073;
  wire __net2072;
  wire __net2071;
  wire __net2070;
  wire [16:0] __net2069;
  wire [255:0] __net2068;
  wire __net2067;
  wire __net2066;
  wire __net2065;
  wire __net2064;
  wire [19:0] __net2063;
  wire [7:0] __net2062;
  wire __stage_FIRST_reset;
  wire __stage_FIRST_set;
  wire __stage_INIT_reset;
  wire __stage_INIT_set;
  wire __stage_CULC_reset;
  wire __stage_CULC_set;
  wire __task_CULC_culc_set;
  wire __task_INIT_init_set;
  wire __task_FIRST_first_set;
  wire __net2061;
  wire __net2060;
  wire __net2059;
  wire __net2058;
  wire __net2057;
  wire __net2056;
  wire __net2055;
  wire __net2054;
  wire __net2053;
  wire __net2052;
  wire __net2051;
  wire __net2050;
  wire __net2049;
  wire __net2048;
  wire __net2047;
  wire __net2046;
  /* sub-modules */
  wire [7:0] _l_val;
  wire [7:0] _l_vol;
  wire [247:0] _l_out0;
  wire [247:0] _l_out1;
  wire _l_pre;
  wire _l_put;
  wire _l_p_reset;
  wire _l_m_clock;
  wire [19:0] _gen_r0out;
  wire [4:0] _gen_num;
  wire [16:0] _gen_in_val;
  wire [4:0] _gen_out_fig0;
  wire [4:0] _gen_out_fig1;
  wire [4:0] _gen_out_fig;
  wire [16:0] _gen_out_val;
  wire _gen_write;
  wire _gen_tournament;
  wire _gen_select;
  wire _gen_p_reset;
  wire _gen_m_clock;
  wire [31:0] _ope_fig;
  wire [7:0] _ope_rand;
  wire [255:0] _ope_gen0;
  wire [255:0] _ope_gen1;
  wire [2:0] _ope_num;
  wire [15:0] _ope_max_vol;
  wire [255:0] _ope_in_val;
  wire [255:0] _ope_in_vol;
  wire [31:0] _ope_out;
  wire [255:0] _ope_gen;
  wire [16:0] _ope_val;
  wire _ope_M_INIT;
  wire _ope_FIT_INIT;
  wire _ope_FIRST;
  wire _ope_CROSS;
  wire _ope_p_reset;
  wire _ope_m_clock;
  wire [31:0] _r0_fig;
  wire [31:0] _r0_out;
  wire _r0_do;
  wire _r0_p_reset;
  wire _r0_m_clock;
  wire [31:0] _r1_fig;
  wire [31:0] _r1_out;
  wire _r1_do;
  wire _r1_p_reset;
  wire _r1_m_clock;
  wire [4:0] _m_fig0;
  wire [4:0] _m_fig1;
  wire [4:0] _m_fig;
  wire [255:0] _m_in;
  wire [255:0] _m_out0;
  wire [255:0] _m_out1;
  wire _m_same_read;
  wire _m_read;
  wire _m_write;
  wire _m_p_reset;
  wire _m_m_clock;
  link l(.p_reset(_l_p_reset),
         .m_clock(_l_m_clock),
         .val(_l_val),
         .vol(_l_vol),
         .out0(_l_out0),
         .out1(_l_out1),
         .pre(_l_pre),
         .put(_l_put));
  gen gen(.p_reset(_gen_p_reset),
          .m_clock(_gen_m_clock),
          .r0out(_gen_r0out),
          .num(_gen_num),
          .in_val(_gen_in_val),
          .out_fig0(_gen_out_fig0),
          .out_fig1(_gen_out_fig1),
          .out_fig(_gen_out_fig),
          .out_val(_gen_out_val),
          .write(_gen_write),
          .tournament(_gen_tournament),
          .select(_gen_select));
  gen_ope ope(.p_reset(_ope_p_reset),
              .m_clock(_ope_m_clock),
              .fig(_ope_fig),
              .rand(_ope_rand),
              .gen0(_ope_gen0),
              .gen1(_ope_gen1),
              .num(_ope_num),
              .max_vol(_ope_max_vol),
              .in_val(_ope_in_val),
              .in_vol(_ope_in_vol),
              .out(_ope_out),
              .gen(_ope_gen),
              .val(_ope_val),
              .M_INIT(_ope_M_INIT),
              .FIT_INIT(_ope_FIT_INIT),
              .FIRST(_ope_FIRST),
              .CROSS(_ope_CROSS));
  rand r0(.p_reset(_r0_p_reset),
          .m_clock(_r0_m_clock),
          .fig(_r0_fig),
          .out(_r0_out),
          .do(_r0_do));
  rand r1(.p_reset(_r1_p_reset),
          .m_clock(_r1_m_clock),
          .fig(_r1_fig),
          .out(_r1_out),
          .do(_r1_do));
  memunit m(.p_reset(_m_p_reset),
            .m_clock(_m_m_clock),
            .fig0(_m_fig0),
            .fig1(_m_fig1),
            .fig(_m_fig),
            .in(_m_in),
            .out0(_m_out0),
            .out1(_m_out1),
            .same_read(_m_same_read),
            .read(_m_read),
            .write(_m_write));
  /* nets */
  assign __net2077 = count == 5'b11111;
  assign __net2078 = _gen_out_fig0 == _gen_out_fig1;
  assign __net2079 = __net2054 | __net2080;
  assign __net2080 = __net2055 | __net2081;
  assign __net2081 = __net2057 | __net2060;
  assign __net2082 = ~__net2047;
  assign __net2083 = __net2055 & __net2056;
  assign __net2084 = __net2055 & __net2085;
  assign __net2085 = ~__net2056;
  assign __net2086 = __net2057 & __net2058;
  assign __net2087 = __net2057 & __net2088;
  assign __net2088 = ~__net2058;
  assign __net2089 = __net2060 & __net2061;
  assign __net2090 = __net2060 & __net2091;
  assign __net2091 = ~__net2061;
  assign __net2092 = __net2049 & __net2050;
  /* invokes */
  assign _gen_p_reset = p_reset;
  assign _gen_m_clock = m_clock;
  assign _gen_tournament = __net2060;
  assign _gen_write = __stage_INIT;
  assign _gen_select = __net2079;
  assign _r0_p_reset = p_reset;
  assign _r0_m_clock = m_clock;
  assign _r0_do = __net2079;
  assign _m_p_reset = p_reset;
  assign _m_m_clock = m_clock;
  assign _m_write = __stage_INIT | __net2060;
  assign _m_same_read = __net2083 | (__net2084 | (__net2086 | (__net2087 | (__net2089 | __net2090))));
  assign _ope_p_reset = p_reset;
  assign _ope_m_clock = m_clock;
  assign _ope_FIRST = __net2092 | __net2049 & __net2076;
  assign _ope_FIT_INIT = __net2064;
  assign _ope_M_INIT = do;
  assign _ope_CROSS = __net2081;
  assign _l_p_reset = p_reset;
  assign _l_m_clock = m_clock;
  assign _l_pre = do | __net2067;
  /* stage operations */
  assign __net2047 = __net2046 ? __net2077 : 1'bx;
  assign __net2048 = __net2064 ? count8 == 3'b111 : 1'bx;
  assign __net2050 = __net2049 ? count_ == 5'b00000 : 1'bx;
  assign __net2051 = count_ == 5'b11111;
  assign __net2052 = count_ == 5'b00010;
  assign __net2053 = __stage_INIT ? __net2077 : 1'bx;
  assign __net2056 = __net2055 ? __net2078 : 1'bx;
  assign __net2058 = __net2057 ? __net2078 : 1'bx;
  assign __net2059 = __net2057 ? count8 == 3'b100 : 1'bx;
  assign __net2061 = __net2060 ? __net2078 : 1'bx;
  /* assigns */
  assign __net2068 = __net2060 ? save_gen : 256'bx;
  assign __net2069 = __net2060 ? save_val : 17'bx;
  assign __net2070 = __stage_INIT & __net2072;
  assign __net2071 = __stage_INIT & __net2074;
  assign __net2072 = __stage_INIT & __net2073;
  assign __net2073 = ~__net2053;
  assign __net2074 = __stage_INIT & __net2053;
  assign __net2075 = __net2049 & __net2076;
  assign __net2076 = ~__net2050;
  assign __net2067 = __net2046 & ~__net2047;
  assign __net2066 = __net2071;
  assign __net2065 = __net2070 | __net2075 & __net2052;
  assign __net2064 = __net2046 & __net2047;
  assign __net2063 = _r0_out[19:0];
  assign __net2062 = _r0_out[27:20];
  assign __net2060 = __stage_CULC_state_reg == __state_CULC__loop & __stage_CULC;
  assign __net2054 = __stage_CULC_state_reg == __state_CULC__init0 & __stage_CULC;
  assign _ope_rand = __net2057 ? __net2062 : __net2060 ? __net2062 : 8'bx;
  assign __net2057 = __stage_CULC_state_reg == __state_CULC__init_loop & __stage_CULC;
  assign _m_in = __stage_INIT ? _ope_gen : __net2068;
  assign val = __net2069;
  assign _gen_r0out = __net2054 ? __net2063 : __net2055 ? __net2063 : __net2057 ? __net2063 : __net2060 ? __net2063 : 20'bx;
  assign __task_FIRST_first_set = do;
  assign __stage_INIT_set = __net2065;
  assign gen_out = __net2068;
  assign _ope_gen1 = __net2057 ? reg_out1 : __net2060 ? reg_out1 : 256'bx;
  assign __task_CULC_culc_set = __net2066;
  assign _gen_num = __stage_INIT ? count : 5'bx;
  assign _m_fig = __stage_INIT ? count : __net2060 ? _gen_out_fig : 5'bx;
  assign _r0_fig = __net2054 ? save_fig : __net2055 ? save_fig : __net2057 ? save_fig : __net2060 ? save_fig : 32'bx;
  assign _m_fig0 = __net2083 ? _gen_out_fig0 : __net2084 ? _gen_out_fig0 : __net2086 ? _gen_out_fig0 : __net2087 ? _gen_out_fig0 : __net2089 ? _gen_out_fig0 : __net2090 ? _gen_out_fig0 : 5'bx;
  assign _ope_max_vol = __net2064 ? MAX_VOL : 16'bx;
  assign _ope_in_vol = __net2064 ? {in_vol, _l_out1} : 256'bx;
  assign _ope_num = __net2064 ? count8 : 3'bx;
  assign _ope_in_val = __net2064 ? {in_val, _l_out0} : 256'bx;
  assign __stage_FIRST_set = do;
  assign __stage_CULC_reset = 1'b0;
  assign __net2049 = __stage_FIRST_state_reg == __state_FIRST__INIT0 & __stage_FIRST;
  assign __net2055 = __stage_CULC_state_reg == __state_CULC__init1 & __stage_CULC;
  assign _gen_in_val = __stage_INIT ? _ope_val : __net2069;
  assign _l_vol = do ? in_vol : __net2067 ? in_vol : 8'bx;
  assign __task_INIT_init_set = __net2065;
  assign __stage_CULC_set = __net2066;
  assign _ope_gen0 = __net2057 ? reg_out0 : __net2060 ? reg_out0 : 256'bx;
  assign __stage_INIT_reset = __net2070 | __net2071;
  assign __net2046 = __stage_FIRST_state_reg == __state_FIRST__INPUT & __stage_FIRST;
  assign _ope_fig = __net2092 ? 32'h8a450abd : __net2075 ? save_fig : 32'bx;
  assign __stage_FIRST_reset = __stage_FIRST & (__net2075 & __net2051);
  assign _l_val = do ? in_val : __net2067 ? in_val : 8'bx;
  /* regUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        count <= 5'b0;
      else if (do)
        count <= 5'b00001;
      else if (__net2046)
        count <= count + 5'b00001;
      else if (__stage_INIT)
        count <= count + 5'b00001;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        save_gen <= 256'b0;
      else if (__net2057 & __net2059)
        save_gen <= _ope_gen;
      else if (__net2060)
        save_gen <= _ope_gen;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        save_val <= 17'b0;
      else if (__net2057 & __net2059)
        save_val <= _ope_val;
      else if (__net2060)
        save_val <= _ope_val;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_out0 <= 256'b0;
      else if (__net2055 & __net2056)
        reg_out0 <= _m_out0;
      else if (__net2055 & ~__net2056)
        reg_out0 <= _m_out0;
      else if (__net2057 & __net2058)
        reg_out0 <= _m_out0;
      else if (__net2057 & ~__net2058)
        reg_out0 <= _m_out0;
      else if (__net2060 & __net2061)
        reg_out0 <= _m_out0;
      else if (__net2060 & ~__net2061)
        reg_out0 <= _m_out0;
    end
  always @(posedge m_clock)
    begin
      if (do)
        MAX_VOL <= max_vol;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        save_fig <= 32'b0;
      else if (__net2049 & __net2050)
        save_fig <= _ope_out;
      else if (__net2049 & ~__net2050)
        save_fig <= _ope_out;
      else if (__stage_INIT & __net2053)
        save_fig <= save_fig;
      else if (__net2054)
        save_fig <= _r0_out;
      else if (__net2055)
        save_fig <= _r0_out;
      else if (__net2057)
        save_fig <= _r0_out;
      else if (__net2060)
        save_fig <= _r0_out;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        count8 <= 3'b0;
      else if (__net2046 & __net2047)
        count8 <= count8 + 3'b001;
      else if (__net2057)
        count8 <= count8 + 3'b001;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        count_ <= 5'b0;
      else if (__net2049)
        count_ <= count_ + 5'b00001;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        reg_out1 <= 256'b0;
      else if (__net2055 & __net2056)
        reg_out1 <= _m_out1;
      else if (__net2055 & ~__net2056)
        reg_out1 <= _m_out1;
      else if (__net2057 & __net2058)
        reg_out1 <= _m_out1;
      else if (__net2057 & ~__net2058)
        reg_out1 <= _m_out1;
      else if (__net2060 & __net2061)
        reg_out1 <= _m_out1;
      else if (__net2060 & ~__net2061)
        reg_out1 <= _m_out1;
    end
  /* stageRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_CULC <= 1'b0;
      else if (__stage_CULC_set | __stage_CULC_reset)
        __stage_CULC <= __stage_CULC_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_INIT <= 1'b0;
      else if (__stage_INIT_set | __stage_INIT_reset)
        __stage_INIT <= __stage_INIT_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_FIRST <= 1'b0;
      else if (__stage_FIRST_set | __stage_FIRST_reset)
        __stage_FIRST <= __stage_FIRST_set;
    end
  /* taskRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_FIRST_first <= 1'b0;
      else if (__task_FIRST_first_set | __stage_FIRST_reset)
        __task_FIRST_first <= __task_FIRST_first_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_INIT_init <= 1'b0;
      else if (__task_INIT_init_set | __stage_INIT_reset)
        __task_INIT_init <= __task_INIT_init_set;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __task_CULC_culc <= 1'b0;
      else if (__task_CULC_culc_set | __stage_CULC_reset)
        __task_CULC_culc <= __task_CULC_culc_set;
    end
  /* stateRegUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_FIRST_state_reg <= __state_FIRST__INPUT;
      else if (__net2046 & __net2047 & __net2048)
        __stage_FIRST_state_reg <= __state_FIRST__INIT0;
      else if (__net2046 & __net2047 & ~__net2048)
        __stage_FIRST_state_reg <= __state_FIRST__INPUT;
      else if (__net2046 & ~__net2047)
        __stage_FIRST_state_reg <= __state_FIRST__INPUT;
      else if (__net2049)
        __stage_FIRST_state_reg <= __state_FIRST__INIT0;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        __stage_CULC_state_reg <= __state_CULC__init0;
      else if (__net2054)
        __stage_CULC_state_reg <= __state_CULC__init1;
      else if (__net2055)
        __stage_CULC_state_reg <= __state_CULC__init_loop;
      else if (__net2057 & __net2059)
        __stage_CULC_state_reg <= __state_CULC__loop;
      else if (__net2057 & ~__net2059)
        __stage_CULC_state_reg <= __state_CULC__init_loop;
      else if (__net2060)
        __stage_CULC_state_reg <= __state_CULC__loop;
    end
endmodule
/* End of file (top.v) */
