/* declare fitness1 */

module fitness4(p_reset, m_clock, init, do, in, in_val, in_vol, val, vol);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input init;
  wire init;
  input do;
  wire do;
  input [3:0] in;
  wire [3:0] in;
  input [31:0] in_val;
  wire [31:0] in_val;
  input [31:0] in_vol;
  wire [31:0] in_vol;
  output [9:0] val;
  wire [9:0] val;
  output [9:0] vol;
  wire [9:0] vol;
  /* elements */
  wire [8:0] sval0;
  wire [8:0] sval1;
  wire [8:0] svol0;
  wire [8:0] svol1;
  /* sub-modules */
  wire _f0_in;
  wire [7:0] _f0_in_val;
  wire [7:0] _f0_in_vol;
  wire [7:0] _f0_val;
  wire [7:0] _f0_vol;
  wire _f0_init;
  wire _f0_do;
  wire _f0_p_reset;
  wire _f0_m_clock;
  wire _f1_in;
  wire [7:0] _f1_in_val;
  wire [7:0] _f1_in_vol;
  wire [7:0] _f1_val;
  wire [7:0] _f1_vol;
  wire _f1_init;
  wire _f1_do;
  wire _f1_p_reset;
  wire _f1_m_clock;
  wire _f2_in;
  wire [7:0] _f2_in_val;
  wire [7:0] _f2_in_vol;
  wire [7:0] _f2_val;
  wire [7:0] _f2_vol;
  wire _f2_init;
  wire _f2_do;
  wire _f2_p_reset;
  wire _f2_m_clock;
  wire _f3_in;
  wire [7:0] _f3_in_val;
  wire [7:0] _f3_in_vol;
  wire [7:0] _f3_val;
  wire [7:0] _f3_vol;
  wire _f3_init;
  wire _f3_do;
  wire _f3_p_reset;
  wire _f3_m_clock;
  fitness1 f0(.p_reset(_f0_p_reset),
              .m_clock(_f0_m_clock),
              .in(_f0_in),
              .in_val(_f0_in_val),
              .in_vol(_f0_in_vol),
              .val(_f0_val),
              .vol(_f0_vol),
              .init(_f0_init),
              .do(_f0_do));
  fitness1 f1(.p_reset(_f1_p_reset),
              .m_clock(_f1_m_clock),
              .in(_f1_in),
              .in_val(_f1_in_val),
              .in_vol(_f1_in_vol),
              .val(_f1_val),
              .vol(_f1_vol),
              .init(_f1_init),
              .do(_f1_do));
  fitness1 f2(.p_reset(_f2_p_reset),
              .m_clock(_f2_m_clock),
              .in(_f2_in),
              .in_val(_f2_in_val),
              .in_vol(_f2_in_vol),
              .val(_f2_val),
              .vol(_f2_vol),
              .init(_f2_init),
              .do(_f2_do));
  fitness1 f3(.p_reset(_f3_p_reset),
              .m_clock(_f3_m_clock),
              .in(_f3_in),
              .in_val(_f3_in_val),
              .in_vol(_f3_in_vol),
              .val(_f3_val),
              .vol(_f3_vol),
              .init(_f3_init),
              .do(_f3_do));
  /* invokes */
  assign _f1_p_reset = p_reset;
  assign _f1_m_clock = m_clock;
  assign _f1_init = init;
  assign _f1_do = do;
  assign _f0_p_reset = p_reset;
  assign _f0_m_clock = m_clock;
  assign _f0_init = init;
  assign _f0_do = do;
  assign _f3_p_reset = p_reset;
  assign _f3_m_clock = m_clock;
  assign _f3_init = init;
  assign _f3_do = do;
  assign _f2_p_reset = p_reset;
  assign _f2_m_clock = m_clock;
  assign _f2_init = init;
  assign _f2_do = do;
  /* assigns */
  assign _f0_in_vol = init ? in_vol[7:0] : 8'bx;
  assign _f1_in = do ? in[1] : 1'bx;
  assign _f0_in_val = init ? in_val[7:0] : 8'bx;
  assign svol0 = do ? 9'b000000000 + {1'b0, _f0_vol + _f1_vol} : 9'bx;
  assign vol = do ? 10'b0000000000 + {1'b0, svol0 + svol1} : 10'bx;
  assign val = do ? 10'b0000000000 + {1'b0, sval0 + sval1} : 10'bx;
  assign _f3_in_vol = init ? in_vol[31:24] : 8'bx;
  assign _f3_in_val = init ? in_val[31:24] : 8'bx;
  assign _f2_in = do ? in[2] : 1'bx;
  assign _f0_in = do ? in[0] : 1'bx;
  assign _f2_in_vol = init ? in_vol[23:16] : 8'bx;
  assign sval1 = do ? 9'b000000000 + {1'b0, _f2_val + _f3_val} : 9'bx;
  assign svol1 = do ? 9'b000000000 + {1'b0, _f2_vol + _f3_vol} : 9'bx;
  assign _f2_in_val = init ? in_val[23:16] : 8'bx;
  assign sval0 = do ? 9'b000000000 + {1'b0, _f0_val + _f1_val} : 9'bx;
  assign _f1_in_vol = init ? in_vol[15:8] : 8'bx;
  assign _f3_in = do ? in[3] : 1'bx;
  assign _f1_in_val = init ? in_val[15:8] : 8'bx;
endmodule
/* End of file (fitness4.v) */
