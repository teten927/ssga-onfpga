sigin clock: None

module comp8(p_reset, m_clock, do, a, b, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [7:0] a;
  wire [7:0] a;
  input [7:0] b;
  wire [7:0] b;
  output out;
  wire out;
  /* elements */
  wire [8:0] tmp;
  wire [8:0] tmp_a;
  /* assigns */
  assign tmp_a = do ? {1'b0, a} : 9'bx;
  assign tmp = do ? tmp_a + ({1'b0, ~b} + 9'b000000001) : 9'bx;
  assign out = do ? tmp[8] : 1'bx;
endmodule
/* End of file (comp8.v) */
