module mem1(p_reset, m_clock, read, write, in, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input read;
  wire read;
  input write;
  wire write;
  input [255:0] in;
  wire [255:0] in;
  output [255:0] out;
  wire [255:0] out;
  /* elements */
  reg [255:0] gen[0:1];
  /* net decls */
  wire __net1484;
  wire [255:0] __net1483;
  wire __net1482;
  /* assigns */
  assign __net1484 = write ? 1'b0 : 1'bx;
  assign __net1483 = read ? gen[__net1482] : 256'bx;
  assign out = read ? __net1483 : 256'bx;
  assign __net1482 = read ? 1'b0 : 1'bx;
  /* memUpdates */
  always @(posedge m_clock)
    begin
      if (write)
        gen[__net1484] <= in;
    end
endmodule
/* End of file (mem1.v) */
