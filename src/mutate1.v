/* declare comp8 */

module mutate1(p_reset, m_clock, do, rand, in, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [7:0] rand;
  wire [7:0] rand;
  input in;
  wire in;
  output out;
  wire out;
  /* net decls */
  wire __net2045;
  /* sub-modules */
  wire [7:0] _c_a;
  wire [7:0] _c_b;
  wire _c_out;
  wire _c_do;
  wire _c_p_reset;
  wire _c_m_clock;
  comp8 c(.p_reset(_c_p_reset),
          .m_clock(_c_m_clock),
          .a(_c_a),
          .b(_c_b),
          .out(_c_out),
          .do(_c_do));
  /* actions */
  assign __net2045 = do ? ~_c_out : 1'bx;
  /* invokes */
  assign _c_p_reset = p_reset;
  assign _c_m_clock = m_clock;
  assign _c_do = do;
  /* assigns */
  assign _c_b = do ? 8'b00000011 : 8'bx;
  assign out = do & ~__net2045 ? in : do & __net2045 ? ~in : 1'bx;
  assign _c_a = do ? rand : 8'bx;
endmodule
/* End of file (mutate1.v) */
