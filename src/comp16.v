module comp16(p_reset, m_clock, do, a, b, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [15:0] a;
  wire [15:0] a;
  input [15:0] b;
  wire [15:0] b;
  output out;
  wire out;
  /* elements */
  wire [16:0] tmp;
  wire [16:0] tmp_a;
  /* assigns */
  assign tmp_a = do ? {1'b0, a} : 17'bx;
  assign tmp = do ? tmp_a + ({1'b0, ~b} + 17'b00000000000000001) : 17'bx;
  assign out = do ? tmp[16] : 1'bx;
endmodule
/* End of file (comp16.v) */
