/* declare comp16 */
/* declare fitness32 */

module fitness256(p_reset, m_clock, init, do0, do1, do2, in, num, max_vol, in_val, in_vol, val);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input init;
  wire init;
  input do0;
  wire do0;
  input do1;
  wire do1;
  input do2;
  wire do2;
  input [255:0] in;
  wire [255:0] in;
  input [2:0] num;
  wire [2:0] num;
  input [15:0] max_vol;
  wire [15:0] max_vol;
  input [255:0] in_val;
  wire [255:0] in_val;
  input [255:0] in_vol;
  wire [255:0] in_vol;
  output [16:0] val;
  wire [16:0] val;
  /* elements */
  wire [13:0] sval0;
  wire [13:0] sval1;
  wire [13:0] sval2;
  wire [13:0] sval3;
  wire [14:0] sval4;
  wire [14:0] sval5;
  wire [13:0] svol0;
  wire [13:0] svol1;
  wire [13:0] svol2;
  wire [13:0] svol3;
  wire [14:0] svol4;
  wire [14:0] svol5;
  reg [15:0] MAX_VOL;
  reg [15:0] val0;
  reg [15:0] vol0;
  /* net decls */
  wire __net1316;
  wire __net1315;
  wire __net1314;
  wire __net1313;
  wire __net1312;
  wire __net1311;
  wire __net1310;
  wire __net1309;
  wire __net1308;
  wire __net1307;
  wire __net1306;
  wire __net1305;
  wire __net1304;
  wire __net1303;
  wire __net1302;
  wire __net1301;
  wire __net1300;
  wire __net1299;
  wire __net1298;
  wire __net1297;
  wire __net1296;
  wire __net1295;
  wire __net1294;
  wire __net1293;
  wire __net1292;
  wire __net1291;
  wire __net1290;
  wire __net1289;
  wire __net1288;
  wire __net1287;
  wire __net1286;
  wire __net1285;
  wire __net1284;
  wire __net1283;
  wire __net1282;
  wire __net1281;
  wire __net1280;
  /* sub-modules */
  wire [31:0] _f0_in;
  wire [255:0] _f0_in_val;
  wire [255:0] _f0_in_vol;
  wire [12:0] _f0_val;
  wire [12:0] _f0_vol;
  wire _f0_init;
  wire _f0_do0;
  wire _f0_do1;
  wire _f0_p_reset;
  wire _f0_m_clock;
  wire [31:0] _f1_in;
  wire [255:0] _f1_in_val;
  wire [255:0] _f1_in_vol;
  wire [12:0] _f1_val;
  wire [12:0] _f1_vol;
  wire _f1_init;
  wire _f1_do0;
  wire _f1_do1;
  wire _f1_p_reset;
  wire _f1_m_clock;
  wire [31:0] _f2_in;
  wire [255:0] _f2_in_val;
  wire [255:0] _f2_in_vol;
  wire [12:0] _f2_val;
  wire [12:0] _f2_vol;
  wire _f2_init;
  wire _f2_do0;
  wire _f2_do1;
  wire _f2_p_reset;
  wire _f2_m_clock;
  wire [31:0] _f3_in;
  wire [255:0] _f3_in_val;
  wire [255:0] _f3_in_vol;
  wire [12:0] _f3_val;
  wire [12:0] _f3_vol;
  wire _f3_init;
  wire _f3_do0;
  wire _f3_do1;
  wire _f3_p_reset;
  wire _f3_m_clock;
  wire [31:0] _f4_in;
  wire [255:0] _f4_in_val;
  wire [255:0] _f4_in_vol;
  wire [12:0] _f4_val;
  wire [12:0] _f4_vol;
  wire _f4_init;
  wire _f4_do0;
  wire _f4_do1;
  wire _f4_p_reset;
  wire _f4_m_clock;
  wire [31:0] _f5_in;
  wire [255:0] _f5_in_val;
  wire [255:0] _f5_in_vol;
  wire [12:0] _f5_val;
  wire [12:0] _f5_vol;
  wire _f5_init;
  wire _f5_do0;
  wire _f5_do1;
  wire _f5_p_reset;
  wire _f5_m_clock;
  wire [31:0] _f6_in;
  wire [255:0] _f6_in_val;
  wire [255:0] _f6_in_vol;
  wire [12:0] _f6_val;
  wire [12:0] _f6_vol;
  wire _f6_init;
  wire _f6_do0;
  wire _f6_do1;
  wire _f6_p_reset;
  wire _f6_m_clock;
  wire [31:0] _f7_in;
  wire [255:0] _f7_in_val;
  wire [255:0] _f7_in_vol;
  wire [12:0] _f7_val;
  wire [12:0] _f7_vol;
  wire _f7_init;
  wire _f7_do0;
  wire _f7_do1;
  wire _f7_p_reset;
  wire _f7_m_clock;
  wire [15:0] _c_a;
  wire [15:0] _c_b;
  wire _c_out;
  wire _c_do;
  wire _c_p_reset;
  wire _c_m_clock;
  fitness32 f0(.p_reset(_f0_p_reset),
               .m_clock(_f0_m_clock),
               .in(_f0_in),
               .in_val(_f0_in_val),
               .in_vol(_f0_in_vol),
               .val(_f0_val),
               .vol(_f0_vol),
               .init(_f0_init),
               .do0(_f0_do0),
               .do1(_f0_do1));
  fitness32 f1(.p_reset(_f1_p_reset),
               .m_clock(_f1_m_clock),
               .in(_f1_in),
               .in_val(_f1_in_val),
               .in_vol(_f1_in_vol),
               .val(_f1_val),
               .vol(_f1_vol),
               .init(_f1_init),
               .do0(_f1_do0),
               .do1(_f1_do1));
  fitness32 f2(.p_reset(_f2_p_reset),
               .m_clock(_f2_m_clock),
               .in(_f2_in),
               .in_val(_f2_in_val),
               .in_vol(_f2_in_vol),
               .val(_f2_val),
               .vol(_f2_vol),
               .init(_f2_init),
               .do0(_f2_do0),
               .do1(_f2_do1));
  fitness32 f3(.p_reset(_f3_p_reset),
               .m_clock(_f3_m_clock),
               .in(_f3_in),
               .in_val(_f3_in_val),
               .in_vol(_f3_in_vol),
               .val(_f3_val),
               .vol(_f3_vol),
               .init(_f3_init),
               .do0(_f3_do0),
               .do1(_f3_do1));
  fitness32 f4(.p_reset(_f4_p_reset),
               .m_clock(_f4_m_clock),
               .in(_f4_in),
               .in_val(_f4_in_val),
               .in_vol(_f4_in_vol),
               .val(_f4_val),
               .vol(_f4_vol),
               .init(_f4_init),
               .do0(_f4_do0),
               .do1(_f4_do1));
  fitness32 f5(.p_reset(_f5_p_reset),
               .m_clock(_f5_m_clock),
               .in(_f5_in),
               .in_val(_f5_in_val),
               .in_vol(_f5_in_vol),
               .val(_f5_val),
               .vol(_f5_vol),
               .init(_f5_init),
               .do0(_f5_do0),
               .do1(_f5_do1));
  fitness32 f6(.p_reset(_f6_p_reset),
               .m_clock(_f6_m_clock),
               .in(_f6_in),
               .in_val(_f6_in_val),
               .in_vol(_f6_in_vol),
               .val(_f6_val),
               .vol(_f6_vol),
               .init(_f6_init),
               .do0(_f6_do0),
               .do1(_f6_do1));
  fitness32 f7(.p_reset(_f7_p_reset),
               .m_clock(_f7_m_clock),
               .in(_f7_in),
               .in_val(_f7_in_val),
               .in_vol(_f7_in_vol),
               .val(_f7_val),
               .vol(_f7_vol),
               .init(_f7_init),
               .do0(_f7_do0),
               .do1(_f7_do1));
  comp16 c(.p_reset(_c_p_reset),
           .m_clock(_c_m_clock),
           .a(_c_a),
           .b(_c_b),
           .out(_c_out),
           .do(_c_do));
  /* actions */
  assign __net1280 = init ? num == 3'b000 : 1'bx;
  assign __net1281 = init ? num == 3'b001 : 1'bx;
  assign __net1282 = init ? num == 3'b010 : 1'bx;
  assign __net1283 = init ? num == 3'b011 : 1'bx;
  assign __net1284 = init ? num == 3'b100 : 1'bx;
  assign __net1285 = init ? num == 3'b101 : 1'bx;
  assign __net1286 = init ? num == 3'b110 : 1'bx;
  assign __net1287 = init ? num == 3'b111 : 1'bx;
  assign __net1288 = do2 ? ~_c_out : 1'bx;
  /* nets */
  assign __net1310 = ~__net1299;
  assign __net1311 = ~__net1304;
  assign __net1312 = ~__net1306;
  assign __net1313 = ~__net1297;
  assign __net1314 = ~__net1300;
  assign __net1315 = ~__net1302;
  assign __net1316 = init & __net1287;
  /* invokes */
  assign _f7_p_reset = p_reset;
  assign _f7_m_clock = m_clock;
  assign _f7_do1 = do1;
  assign _f7_init = ~__net1308 & (__net1314 & (__net1315 & (__net1311 & (__net1312 & (__net1313 & (__net1310 & __net1316))))));
  assign _f7_do0 = do0;
  assign _f1_p_reset = p_reset;
  assign _f1_m_clock = m_clock;
  assign _f1_do1 = do1;
  assign _f1_init = __net1310 & __net1297;
  assign _f1_do0 = do0;
  assign _f4_p_reset = p_reset;
  assign _f4_m_clock = m_clock;
  assign _f4_do1 = do1;
  assign _f4_init = __net1311 & (__net1312 & (__net1313 & (__net1310 & __net1302)));
  assign _f4_do0 = do0;
  assign _f6_p_reset = p_reset;
  assign _f6_m_clock = m_clock;
  assign _f6_do1 = do1;
  assign _f6_init = __net1314 & (__net1315 & (__net1311 & (__net1312 & (__net1313 & (__net1310 & __net1308)))));
  assign _f6_do0 = do0;
  assign _f0_p_reset = p_reset;
  assign _f0_m_clock = m_clock;
  assign _f0_do1 = do1;
  assign _f0_init = __net1299;
  assign _f0_do0 = do0;
  assign _f3_p_reset = p_reset;
  assign _f3_m_clock = m_clock;
  assign _f3_do1 = do1;
  assign _f3_init = __net1312 & (__net1313 & (__net1310 & __net1304));
  assign _f3_do0 = do0;
  assign _c_p_reset = p_reset;
  assign _c_m_clock = m_clock;
  assign _c_do = do2;
  assign _f2_p_reset = p_reset;
  assign _f2_m_clock = m_clock;
  assign _f2_do1 = do1;
  assign _f2_init = __net1313 & (__net1310 & __net1306);
  assign _f2_do0 = do0;
  assign _f5_p_reset = p_reset;
  assign _f5_m_clock = m_clock;
  assign _f5_do1 = do1;
  assign _f5_init = __net1315 & (__net1311 & (__net1312 & (__net1313 & (__net1310 & __net1300))));
  assign _f5_do0 = do0;
  /* assigns */
  assign __net1297 = init & __net1281;
  assign __net1298 = ~__net1299;
  assign __net1299 = init & __net1280;
  assign __net1300 = init & __net1285;
  assign __net1301 = ~__net1302;
  assign __net1302 = init & __net1284;
  assign __net1303 = ~__net1304;
  assign __net1304 = init & __net1283;
  assign __net1305 = ~__net1306;
  assign __net1306 = init & __net1282;
  assign __net1307 = ~__net1297;
  assign __net1308 = init & __net1286;
  assign __net1309 = ~__net1300;
  assign __net1296 = __net1298 & __net1297;
  assign __net1295 = __net1301 & (__net1303 & (__net1305 & (__net1307 & (__net1298 & __net1300))));
  assign __net1294 = __net1309 & (__net1301 & (__net1303 & (__net1305 & (__net1307 & (__net1298 & __net1308)))));
  assign __net1293 = __net1307 & (__net1298 & __net1306);
  assign __net1292 = ~__net1308 & (__net1309 & (__net1301 & (__net1303 & (__net1305 & (__net1307 & (__net1298 & __net1316))))));
  assign __net1291 = __net1305 & (__net1307 & (__net1298 & __net1304));
  assign __net1290 = __net1303 & (__net1305 & (__net1307 & (__net1298 & __net1302)));
  assign __net1289 = __net1299;
  assign _f5_in_val = __net1295 ? in_val : 256'bx;
  assign _f0_in_vol = __net1289 ? in_vol : 256'bx;
  assign _f4_in_vol = __net1290 ? in_vol : 256'bx;
  assign _f1_in = do0 ? in[63:32] : 32'bx;
  assign _f0_in_val = __net1289 ? in_val : 256'bx;
  assign sval5 = do1 ? 15'b000000000000000 + {1'b0, sval2 + sval3} : 15'bx;
  assign svol0 = do1 ? 14'b00000000000000 + {1'b0, _f0_vol + _f1_vol} : 14'bx;
  assign svol3 = do1 ? 14'b00000000000000 + {1'b0, _f6_vol + _f7_vol} : 14'bx;
  assign _f4_in_val = __net1290 ? in_val : 256'bx;
  assign sval2 = do1 ? 14'b00000000000000 + {1'b0, _f4_val + _f5_val} : 14'bx;
  assign val = do2 & ~__net1288 ? {1'b0, val0} : do2 & __net1288 ? {1'b0, MAX_VOL} + ({1'b1, ~vol0} + 17'b00000000000000001) : 17'bx;
  assign _f6_in = do0 ? in[223:192] : 32'bx;
  assign svol2 = do1 ? 14'b00000000000000 + {1'b0, _f4_vol + _f5_vol} : 14'bx;
  assign _f4_in = do0 ? in[159:128] : 32'bx;
  assign _f3_in_vol = __net1291 ? in_vol : 256'bx;
  assign svol5 = do1 ? 15'b000000000000000 + {1'b0, svol2 + svol3} : 15'bx;
  assign sval4 = do1 ? 15'b000000000000000 + {1'b0, sval0 + sval1} : 15'bx;
  assign _f7_in_vol = __net1292 ? in_vol : 256'bx;
  assign _c_b = do2 ? vol0 : 16'bx;
  assign _f3_in_val = __net1291 ? in_val : 256'bx;
  assign _f2_in = do0 ? in[95:64] : 32'bx;
  assign _f0_in = do0 ? in[31:0] : 32'bx;
  assign _f2_in_vol = __net1293 ? in_vol : 256'bx;
  assign sval1 = do1 ? 14'b00000000000000 + {1'b0, _f2_val + _f3_val} : 14'bx;
  assign _f7_in_val = __net1292 ? in_val : 256'bx;
  assign _f6_in_vol = __net1294 ? in_vol : 256'bx;
  assign svol1 = do1 ? 14'b00000000000000 + {1'b0, _f2_vol + _f3_vol} : 14'bx;
  assign _f2_in_val = __net1293 ? in_val : 256'bx;
  assign sval0 = do1 ? 14'b00000000000000 + {1'b0, _f0_val + _f1_val} : 14'bx;
  assign svol4 = do1 ? 15'b000000000000000 + {1'b0, svol0 + sval1} : 15'bx;
  assign _f1_in_vol = __net1296 ? in_vol : 256'bx;
  assign _f6_in_val = __net1294 ? in_val : 256'bx;
  assign _f5_in_vol = __net1295 ? in_vol : 256'bx;
  assign _f3_in = do0 ? in[127:96] : 32'bx;
  assign sval3 = do1 ? 14'b00000000000000 + {1'b0, _f6_val + _f7_val} : 14'bx;
  assign _f7_in = do0 ? in[255:224] : 32'bx;
  assign _f5_in = do0 ? in[191:160] : 32'bx;
  assign _c_a = do2 ? MAX_VOL : 16'bx;
  assign _f1_in_val = __net1296 ? in_val : 256'bx;
  /* regUpdates */
  always @(posedge m_clock)
    begin
      if (init & __net1280)
        MAX_VOL <= max_vol;
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val0 <= 16'b0;
      else if (do1)
        val0 <= 16'b0000000000000000 + {1'b0, sval4 + sval5};
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        vol0 <= 16'b0;
      else if (do1)
        vol0 <= 16'b0000000000000000 + {1'b0, svol4 + svol5};
    end
endmodule
/* End of file (fitness256.v) */
