/* declare mutate1 */
/* declare rand */

module mutate4(p_reset, m_clock, do, fig, in, out_fig, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [31:0] fig;
  wire [31:0] fig;
  input [3:0] in;
  wire [3:0] in;
  output [31:0] out_fig;
  wire [31:0] out_fig;
  output [3:0] out;
  wire [3:0] out;
  /* elements */
  wire [31:0] sub;
  /* sub-modules */
  wire [31:0] _r_fig;
  wire [31:0] _r_out;
  wire _r_do;
  wire _r_p_reset;
  wire _r_m_clock;
  wire [7:0] _m0_rand;
  wire _m0_in;
  wire _m0_out;
  wire _m0_do;
  wire _m0_p_reset;
  wire _m0_m_clock;
  wire [7:0] _m1_rand;
  wire _m1_in;
  wire _m1_out;
  wire _m1_do;
  wire _m1_p_reset;
  wire _m1_m_clock;
  wire [7:0] _m2_rand;
  wire _m2_in;
  wire _m2_out;
  wire _m2_do;
  wire _m2_p_reset;
  wire _m2_m_clock;
  wire [7:0] _m3_rand;
  wire _m3_in;
  wire _m3_out;
  wire _m3_do;
  wire _m3_p_reset;
  wire _m3_m_clock;
  rand r(.p_reset(_r_p_reset),
         .m_clock(_r_m_clock),
         .fig(_r_fig),
         .out(_r_out),
         .do(_r_do));
  mutate1 m0(.p_reset(_m0_p_reset),
             .m_clock(_m0_m_clock),
             .rand(_m0_rand),
             .in(_m0_in),
             .out(_m0_out),
             .do(_m0_do));
  mutate1 m1(.p_reset(_m1_p_reset),
             .m_clock(_m1_m_clock),
             .rand(_m1_rand),
             .in(_m1_in),
             .out(_m1_out),
             .do(_m1_do));
  mutate1 m2(.p_reset(_m2_p_reset),
             .m_clock(_m2_m_clock),
             .rand(_m2_rand),
             .in(_m2_in),
             .out(_m2_out),
             .do(_m2_do));
  mutate1 m3(.p_reset(_m3_p_reset),
             .m_clock(_m3_m_clock),
             .rand(_m3_rand),
             .in(_m3_in),
             .out(_m3_out),
             .do(_m3_do));
  /* invokes */
  assign _m3_p_reset = p_reset;
  assign _m3_m_clock = m_clock;
  assign _m3_do = do;
  assign _m2_p_reset = p_reset;
  assign _m2_m_clock = m_clock;
  assign _m2_do = do;
  assign _m1_p_reset = p_reset;
  assign _m1_m_clock = m_clock;
  assign _m1_do = do;
  assign _r_p_reset = p_reset;
  assign _r_m_clock = m_clock;
  assign _r_do = do;
  assign _m0_p_reset = p_reset;
  assign _m0_m_clock = m_clock;
  assign _m0_do = do;
  /* assigns */
  assign out_fig = do ? sub : 32'bx;
  assign _m0_rand = do ? sub[31:24] : 8'bx;
  assign sub = do ? _r_out : 32'bx;
  assign _m2_in = do ? in[2] : 1'bx;
  assign _m0_in = do ? in[0] : 1'bx;
  assign _m1_rand = do ? sub[23:16] : 8'bx;
  assign out = do ? {_m3_out, {_m2_out, {_m1_out, _m0_out}}} : 4'bx;
  assign _r_fig = do ? fig : 32'bx;
  assign _m2_rand = do ? sub[15:8] : 8'bx;
  assign _m1_in = do ? in[1] : 1'bx;
  assign _m3_in = do ? in[3] : 1'bx;
  assign _m3_rand = do ? sub[7:0] : 8'bx;
endmodule
/* End of file (mutate4.v) */
