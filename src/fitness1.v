module fitness1(p_reset, m_clock, init, do, in, in_val, in_vol, val, vol);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input init;
  wire init;
  input do;
  wire do;
  input in;
  wire in;
  input [7:0] in_val;
  wire [7:0] in_val;
  input [7:0] in_vol;
  wire [7:0] in_vol;
  output [7:0] val;
  wire [7:0] val;
  output [7:0] vol;
  wire [7:0] vol;
  /* elements */
  reg [7:0] value;
  reg [7:0] volume;
  /* net decls */
  wire __net1279;
  wire __net1278;
  wire __net1277;
  /* actions */
  assign __net1277 = do ? in == 1'b1 : 1'bx;
  /* assigns */
  assign __net1279 = do & __net1277;
  assign __net1278 = do & ~__net1277;
  assign vol = __net1278 ? 8'h00 : __net1279 ? volume : 8'bx;
  assign val = __net1278 ? 8'h00 : __net1279 ? value : 8'bx;
  /* regUpdates */
  always @(posedge m_clock)
    begin
      if (init)
        value <= in_val;
    end
  always @(posedge m_clock)
    begin
      if (init)
        volume <= in_vol;
    end
endmodule
/* End of file (fitness1.v) */
