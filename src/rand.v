/* declare rand_min */

module rand(p_reset, m_clock, do, fig, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [31:0] fig;
  wire [31:0] fig;
  output [31:0] out;
  wire [31:0] out;
  /* sub-modules */
  wire [3:0] _r0_fig;
  wire _r0_in1;
  wire _r0_in2;
  wire [3:0] _r0_out;
  wire _r0_do;
  wire _r0_p_reset;
  wire _r0_m_clock;
  wire [3:0] _r1_fig;
  wire _r1_in1;
  wire _r1_in2;
  wire [3:0] _r1_out;
  wire _r1_do;
  wire _r1_p_reset;
  wire _r1_m_clock;
  wire [3:0] _r2_fig;
  wire _r2_in1;
  wire _r2_in2;
  wire [3:0] _r2_out;
  wire _r2_do;
  wire _r2_p_reset;
  wire _r2_m_clock;
  wire [3:0] _r3_fig;
  wire _r3_in1;
  wire _r3_in2;
  wire [3:0] _r3_out;
  wire _r3_do;
  wire _r3_p_reset;
  wire _r3_m_clock;
  wire [3:0] _r4_fig;
  wire _r4_in1;
  wire _r4_in2;
  wire [3:0] _r4_out;
  wire _r4_do;
  wire _r4_p_reset;
  wire _r4_m_clock;
  wire [3:0] _r5_fig;
  wire _r5_in1;
  wire _r5_in2;
  wire [3:0] _r5_out;
  wire _r5_do;
  wire _r5_p_reset;
  wire _r5_m_clock;
  wire [3:0] _r6_fig;
  wire _r6_in1;
  wire _r6_in2;
  wire [3:0] _r6_out;
  wire _r6_do;
  wire _r6_p_reset;
  wire _r6_m_clock;
  wire [3:0] _r7_fig;
  wire _r7_in1;
  wire _r7_in2;
  wire [3:0] _r7_out;
  wire _r7_do;
  wire _r7_p_reset;
  wire _r7_m_clock;
  rand_min r0(.p_reset(_r0_p_reset),
              .m_clock(_r0_m_clock),
              .fig(_r0_fig),
              .in1(_r0_in1),
              .in2(_r0_in2),
              .out(_r0_out),
              .do(_r0_do));
  rand_min r1(.p_reset(_r1_p_reset),
              .m_clock(_r1_m_clock),
              .fig(_r1_fig),
              .in1(_r1_in1),
              .in2(_r1_in2),
              .out(_r1_out),
              .do(_r1_do));
  rand_min r2(.p_reset(_r2_p_reset),
              .m_clock(_r2_m_clock),
              .fig(_r2_fig),
              .in1(_r2_in1),
              .in2(_r2_in2),
              .out(_r2_out),
              .do(_r2_do));
  rand_min r3(.p_reset(_r3_p_reset),
              .m_clock(_r3_m_clock),
              .fig(_r3_fig),
              .in1(_r3_in1),
              .in2(_r3_in2),
              .out(_r3_out),
              .do(_r3_do));
  rand_min r4(.p_reset(_r4_p_reset),
              .m_clock(_r4_m_clock),
              .fig(_r4_fig),
              .in1(_r4_in1),
              .in2(_r4_in2),
              .out(_r4_out),
              .do(_r4_do));
  rand_min r5(.p_reset(_r5_p_reset),
              .m_clock(_r5_m_clock),
              .fig(_r5_fig),
              .in1(_r5_in1),
              .in2(_r5_in2),
              .out(_r5_out),
              .do(_r5_do));
  rand_min r6(.p_reset(_r6_p_reset),
              .m_clock(_r6_m_clock),
              .fig(_r6_fig),
              .in1(_r6_in1),
              .in2(_r6_in2),
              .out(_r6_out),
              .do(_r6_do));
  rand_min r7(.p_reset(_r7_p_reset),
              .m_clock(_r7_m_clock),
              .fig(_r7_fig),
              .in1(_r7_in1),
              .in2(_r7_in2),
              .out(_r7_out),
              .do(_r7_do));
  /* invokes */
  assign _r3_p_reset = p_reset;
  assign _r3_m_clock = m_clock;
  assign _r3_do = do;
  assign _r6_p_reset = p_reset;
  assign _r6_m_clock = m_clock;
  assign _r6_do = do;
  assign _r0_p_reset = p_reset;
  assign _r0_m_clock = m_clock;
  assign _r0_do = do;
  assign _r2_p_reset = p_reset;
  assign _r2_m_clock = m_clock;
  assign _r2_do = do;
  assign _r5_p_reset = p_reset;
  assign _r5_m_clock = m_clock;
  assign _r5_do = do;
  assign _r4_p_reset = p_reset;
  assign _r4_m_clock = m_clock;
  assign _r4_do = do;
  assign _r7_p_reset = p_reset;
  assign _r7_m_clock = m_clock;
  assign _r7_do = do;
  assign _r1_p_reset = p_reset;
  assign _r1_m_clock = m_clock;
  assign _r1_do = do;
  /* assigns */
  assign _r5_in1 = do ? fig[12] : 1'bx;
  assign _r2_fig = do ? fig[23:20] : 4'bx;
  assign _r3_in2 = do ? fig[15] : 1'bx;
  assign _r6_fig = do ? fig[7:4] : 4'bx;
  assign _r7_in2 = do ? 1'b0 : 1'bx;
  assign _r2_in1 = do ? fig[24] : 1'bx;
  assign _r3_fig = do ? fig[19:16] : 4'bx;
  assign _r7_fig = do ? fig[3:0] : 4'bx;
  assign _r6_in1 = do ? fig[8] : 1'bx;
  assign _r0_in2 = do ? fig[27] : 1'bx;
  assign _r4_in2 = do ? fig[11] : 1'bx;
  assign _r0_fig = do ? fig[31:28] : 4'bx;
  assign _r3_in1 = do ? fig[20] : 1'bx;
  assign _r4_fig = do ? fig[15:12] : 4'bx;
  assign _r7_in1 = do ? fig[4] : 1'bx;
  assign out = do ? {_r0_out, {_r1_out, {_r2_out, {_r3_out, {_r4_out, {_r5_out, {_r6_out, _r7_out}}}}}}} : 32'bx;
  assign _r1_in2 = do ? fig[23] : 1'bx;
  assign _r5_in2 = do ? fig[7] : 1'bx;
  assign _r1_fig = do ? fig[27:24] : 4'bx;
  assign _r0_in1 = do ? fig[31] : 1'bx;
  assign _r5_fig = do ? fig[11:8] : 4'bx;
  assign _r4_in1 = do ? fig[16] : 1'bx;
  assign _r2_in2 = do ? fig[19] : 1'bx;
  assign _r6_in2 = do ? fig[3] : 1'bx;
  assign _r1_in1 = do ? fig[28] : 1'bx;
endmodule
/* End of file (rand.v) */
