/* declare comp16_sign */

module tournament1(p_reset, m_clock, do, do2, in_fig0, in_fig1, in_val0, in_val1, val0, val1, fig0, fig1);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input do2;
  wire do2;
  input [4:0] in_fig0;
  wire [4:0] in_fig0;
  input [4:0] in_fig1;
  wire [4:0] in_fig1;
  input [16:0] in_val0;
  wire [16:0] in_val0;
  input [16:0] in_val1;
  wire [16:0] in_val1;
  output [16:0] val0;
  wire [16:0] val0;
  output [16:0] val1;
  wire [16:0] val1;
  output [4:0] fig0;
  wire [4:0] fig0;
  output [4:0] fig1;
  wire [4:0] fig1;
  /* net decls */
  wire __net2098;
  wire __net2097;
  wire __net2096;
  wire __net2095;
  wire __net2094;
  wire __net2093;
  /* sub-modules */
  wire [16:0] _c1_a;
  wire [16:0] _c1_b;
  wire _c1_out;
  wire _c1_do;
  wire _c1_p_reset;
  wire _c1_m_clock;
  wire [16:0] _c2_a;
  wire [16:0] _c2_b;
  wire _c2_out;
  wire _c2_do;
  wire _c2_p_reset;
  wire _c2_m_clock;
  comp16_sign c1(.p_reset(_c1_p_reset),
                 .m_clock(_c1_m_clock),
                 .a(_c1_a),
                 .b(_c1_b),
                 .out(_c1_out),
                 .do(_c1_do));
  comp16_sign c2(.p_reset(_c2_p_reset),
                 .m_clock(_c2_m_clock),
                 .a(_c2_a),
                 .b(_c2_b),
                 .out(_c2_out),
                 .do(_c2_do));
  /* actions */
  assign __net2093 = do ? ~_c1_out : 1'bx;
  assign __net2094 = do2 ? ~_c2_out : 1'bx;
  /* invokes */
  assign _c1_p_reset = p_reset;
  assign _c1_m_clock = m_clock;
  assign _c1_do = do;
  assign _c2_p_reset = p_reset;
  assign _c2_m_clock = m_clock;
  assign _c2_do = do2;
  /* assigns */
  assign __net2098 = do2 & __net2094;
  assign __net2097 = do2 & ~__net2094;
  assign __net2096 = do & __net2093;
  assign __net2095 = do & ~__net2093;
  assign val1 = __net2095 ? in_val0 : __net2096 ? in_val1 : 17'bx;
  assign _c2_b = do2 ? in_val1 : 17'bx;
  assign fig1 = __net2095 ? in_fig0 : __net2096 ? in_fig1 : 5'bx;
  assign val0 = __net2097 ? in_val0 : __net2098 ? in_val1 : __net2095 ? in_val1 : __net2096 ? in_val0 : 17'bx;
  assign _c1_b = do ? in_val1 : 17'bx;
  assign _c2_a = do2 ? in_val0 : 17'bx;
  assign _c1_a = do ? in_val0 : 17'bx;
  assign fig0 = __net2097 ? in_fig0 : __net2098 ? in_fig1 : __net2095 ? in_fig1 : __net2096 ? in_fig0 : 5'bx;
endmodule
/* End of file (tournament1.v) */
