module rand_min(p_reset, m_clock, do, fig, in1, in2, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [3:0] fig;
  wire [3:0] fig;
  input in1;
  wire in1;
  input in2;
  wire in2;
  output [3:0] out;
  wire [3:0] out;
  /* elements */
  wire sel0;
  wire sel1;
  wire sel2;
  wire sel3;
  /* assigns */
  assign sel1 = do ? fig[0] ^ fig[2] : 1'bx;
  assign sel0 = do ? in2 ^ (fig[0] ^ fig[1]) : 1'bx;
  assign out = do ? {sel3, {sel2, {sel1, sel0}}} : 4'bx;
  assign sel3 = do ? fig[2] ^ in1 : 1'bx;
  assign sel2 = do ? fig[1] ^ (fig[2] ^ fig[3]) : 1'bx;
endmodule
/* End of file (rand_min.v) */
