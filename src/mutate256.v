/* declare mutate32 */

module mutate256(p_reset, m_clock, init, do, in, out_fig, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input init;
  wire init;
  input do;
  wire do;
  input [255:0] in;
  wire [255:0] in;
  output [31:0] out_fig;
  wire [31:0] out_fig;
  output [255:0] out;
  wire [255:0] out;
  /* elements */
  reg [255:0] save_fig;
  /* sub-modules */
  wire [255:0] _m0_fig;
  wire [31:0] _m0_in;
  wire [255:0] _m0_out_fig;
  wire [31:0] _m0_out;
  wire _m0_do;
  wire _m0_p_reset;
  wire _m0_m_clock;
  wire [255:0] _m1_fig;
  wire [31:0] _m1_in;
  wire [255:0] _m1_out_fig;
  wire [31:0] _m1_out;
  wire _m1_do;
  wire _m1_p_reset;
  wire _m1_m_clock;
  wire [255:0] _m2_fig;
  wire [31:0] _m2_in;
  wire [255:0] _m2_out_fig;
  wire [31:0] _m2_out;
  wire _m2_do;
  wire _m2_p_reset;
  wire _m2_m_clock;
  wire [255:0] _m3_fig;
  wire [31:0] _m3_in;
  wire [255:0] _m3_out_fig;
  wire [31:0] _m3_out;
  wire _m3_do;
  wire _m3_p_reset;
  wire _m3_m_clock;
  wire [255:0] _m4_fig;
  wire [31:0] _m4_in;
  wire [255:0] _m4_out_fig;
  wire [31:0] _m4_out;
  wire _m4_do;
  wire _m4_p_reset;
  wire _m4_m_clock;
  wire [255:0] _m5_fig;
  wire [31:0] _m5_in;
  wire [255:0] _m5_out_fig;
  wire [31:0] _m5_out;
  wire _m5_do;
  wire _m5_p_reset;
  wire _m5_m_clock;
  wire [255:0] _m6_fig;
  wire [31:0] _m6_in;
  wire [255:0] _m6_out_fig;
  wire [31:0] _m6_out;
  wire _m6_do;
  wire _m6_p_reset;
  wire _m6_m_clock;
  wire [255:0] _m7_fig;
  wire [31:0] _m7_in;
  wire [255:0] _m7_out_fig;
  wire [31:0] _m7_out;
  wire _m7_do;
  wire _m7_p_reset;
  wire _m7_m_clock;
  mutate32 m0(.p_reset(_m0_p_reset),
              .m_clock(_m0_m_clock),
              .fig(_m0_fig),
              .in(_m0_in),
              .out_fig(_m0_out_fig),
              .out(_m0_out),
              .do(_m0_do));
  mutate32 m1(.p_reset(_m1_p_reset),
              .m_clock(_m1_m_clock),
              .fig(_m1_fig),
              .in(_m1_in),
              .out_fig(_m1_out_fig),
              .out(_m1_out),
              .do(_m1_do));
  mutate32 m2(.p_reset(_m2_p_reset),
              .m_clock(_m2_m_clock),
              .fig(_m2_fig),
              .in(_m2_in),
              .out_fig(_m2_out_fig),
              .out(_m2_out),
              .do(_m2_do));
  mutate32 m3(.p_reset(_m3_p_reset),
              .m_clock(_m3_m_clock),
              .fig(_m3_fig),
              .in(_m3_in),
              .out_fig(_m3_out_fig),
              .out(_m3_out),
              .do(_m3_do));
  mutate32 m4(.p_reset(_m4_p_reset),
              .m_clock(_m4_m_clock),
              .fig(_m4_fig),
              .in(_m4_in),
              .out_fig(_m4_out_fig),
              .out(_m4_out),
              .do(_m4_do));
  mutate32 m5(.p_reset(_m5_p_reset),
              .m_clock(_m5_m_clock),
              .fig(_m5_fig),
              .in(_m5_in),
              .out_fig(_m5_out_fig),
              .out(_m5_out),
              .do(_m5_do));
  mutate32 m6(.p_reset(_m6_p_reset),
              .m_clock(_m6_m_clock),
              .fig(_m6_fig),
              .in(_m6_in),
              .out_fig(_m6_out_fig),
              .out(_m6_out),
              .do(_m6_do));
  mutate32 m7(.p_reset(_m7_p_reset),
              .m_clock(_m7_m_clock),
              .fig(_m7_fig),
              .in(_m7_in),
              .out_fig(_m7_out_fig),
              .out(_m7_out),
              .do(_m7_do));
  /* invokes */
  assign _m3_p_reset = p_reset;
  assign _m3_m_clock = m_clock;
  assign _m3_do = do;
  assign _m2_p_reset = p_reset;
  assign _m2_m_clock = m_clock;
  assign _m2_do = do;
  assign _m5_p_reset = p_reset;
  assign _m5_m_clock = m_clock;
  assign _m5_do = do;
  assign _m7_p_reset = p_reset;
  assign _m7_m_clock = m_clock;
  assign _m7_do = do;
  assign _m1_p_reset = p_reset;
  assign _m1_m_clock = m_clock;
  assign _m1_do = do;
  assign _m4_p_reset = p_reset;
  assign _m4_m_clock = m_clock;
  assign _m4_do = do;
  assign _m6_p_reset = p_reset;
  assign _m6_m_clock = m_clock;
  assign _m6_do = do;
  assign _m0_p_reset = p_reset;
  assign _m0_m_clock = m_clock;
  assign _m0_do = do;
  /* assigns */
  assign _m2_fig = do ? _m3_out_fig : 256'bx;
  assign _m6_fig = do ? _m7_out_fig : 256'bx;
  assign _m3_fig = do ? _m4_out_fig : 256'bx;
  assign _m7_fig = do ? save_fig : 256'bx;
  assign _m4_in = do ? in[159:128] : 32'bx;
  assign _m2_in = do ? in[95:64] : 32'bx;
  assign _m6_in = do ? in[223:192] : 32'bx;
  assign _m0_in = do ? in[31:0] : 32'bx;
  assign _m0_fig = do ? _m1_out_fig : 256'bx;
  assign _m4_fig = do ? _m5_out_fig : 256'bx;
  assign out = do ? {_m7_out, {_m6_out, {_m5_out, {_m4_out, {_m3_out, {_m2_out, {_m1_out, _m0_out}}}}}}} : 256'bx;
  assign _m5_in = do ? in[191:160] : 32'bx;
  assign _m1_in = do ? in[63:32] : 32'bx;
  assign _m1_fig = do ? _m2_out_fig : 256'bx;
  assign _m3_in = do ? in[127:96] : 32'bx;
  assign _m7_in = do ? in[255:224] : 32'bx;
  assign _m5_fig = do ? _m6_out_fig : 256'bx;
  /* regUpdates */
  always @(posedge m_clock)
    begin
      if (init)
        save_fig <= 256'hf8652937b3750d8d067c48b52c8b86d7f4c1df9830d3d50ec7a0f5511dca4a13;
      else if (do)
        save_fig <= _m0_out_fig;
    end
endmodule
/* End of file (mutate256.v) */
