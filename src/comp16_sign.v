module comp16_sign(p_reset, m_clock, do, a, b, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [16:0] a;
  wire [16:0] a;
  input [16:0] b;
  wire [16:0] b;
  output out;
  wire out;
  /* elements */
  wire [17:0] tmp;
  wire [17:0] tmp_a;
  wire [16:0] tmp_b;
  /* assigns */
  assign tmp_a = do ? {1'b0, {~a[16], a[15:0]}} : 18'bx;
  assign tmp = do ? tmp_a + ({1'b0, ~tmp_b} + 18'b000000000000000001) : 18'bx;
  assign out = do ? tmp[17] : 1'bx;
  assign tmp_b = do ? {~b[16], b[15:0]} : 17'bx;
endmodule
/* End of file (comp16_sign.v) */
