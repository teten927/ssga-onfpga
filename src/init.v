/* declare rand */

module init(p_reset, m_clock, do, fig, out_gen, out);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input do;
  wire do;
  input [31:0] fig;
  wire [31:0] fig;
  output [255:0] out_gen;
  wire [255:0] out_gen;
  output [31:0] out;
  wire [31:0] out;
  /* sub-modules */
  wire [31:0] _r0_fig;
  wire [31:0] _r0_out;
  wire _r0_do;
  wire _r0_p_reset;
  wire _r0_m_clock;
  wire [31:0] _r1_fig;
  wire [31:0] _r1_out;
  wire _r1_do;
  wire _r1_p_reset;
  wire _r1_m_clock;
  wire [31:0] _r2_fig;
  wire [31:0] _r2_out;
  wire _r2_do;
  wire _r2_p_reset;
  wire _r2_m_clock;
  wire [31:0] _r3_fig;
  wire [31:0] _r3_out;
  wire _r3_do;
  wire _r3_p_reset;
  wire _r3_m_clock;
  wire [31:0] _r4_fig;
  wire [31:0] _r4_out;
  wire _r4_do;
  wire _r4_p_reset;
  wire _r4_m_clock;
  wire [31:0] _r5_fig;
  wire [31:0] _r5_out;
  wire _r5_do;
  wire _r5_p_reset;
  wire _r5_m_clock;
  wire [31:0] _r6_fig;
  wire [31:0] _r6_out;
  wire _r6_do;
  wire _r6_p_reset;
  wire _r6_m_clock;
  wire [31:0] _r7_fig;
  wire [31:0] _r7_out;
  wire _r7_do;
  wire _r7_p_reset;
  wire _r7_m_clock;
  rand r0(.p_reset(_r0_p_reset),
          .m_clock(_r0_m_clock),
          .fig(_r0_fig),
          .out(_r0_out),
          .do(_r0_do));
  rand r1(.p_reset(_r1_p_reset),
          .m_clock(_r1_m_clock),
          .fig(_r1_fig),
          .out(_r1_out),
          .do(_r1_do));
  rand r2(.p_reset(_r2_p_reset),
          .m_clock(_r2_m_clock),
          .fig(_r2_fig),
          .out(_r2_out),
          .do(_r2_do));
  rand r3(.p_reset(_r3_p_reset),
          .m_clock(_r3_m_clock),
          .fig(_r3_fig),
          .out(_r3_out),
          .do(_r3_do));
  rand r4(.p_reset(_r4_p_reset),
          .m_clock(_r4_m_clock),
          .fig(_r4_fig),
          .out(_r4_out),
          .do(_r4_do));
  rand r5(.p_reset(_r5_p_reset),
          .m_clock(_r5_m_clock),
          .fig(_r5_fig),
          .out(_r5_out),
          .do(_r5_do));
  rand r6(.p_reset(_r6_p_reset),
          .m_clock(_r6_m_clock),
          .fig(_r6_fig),
          .out(_r6_out),
          .do(_r6_do));
  rand r7(.p_reset(_r7_p_reset),
          .m_clock(_r7_m_clock),
          .fig(_r7_fig),
          .out(_r7_out),
          .do(_r7_do));
  /* invokes */
  assign _r3_p_reset = p_reset;
  assign _r3_m_clock = m_clock;
  assign _r3_do = do;
  assign _r6_p_reset = p_reset;
  assign _r6_m_clock = m_clock;
  assign _r6_do = do;
  assign _r0_p_reset = p_reset;
  assign _r0_m_clock = m_clock;
  assign _r0_do = do;
  assign _r2_p_reset = p_reset;
  assign _r2_m_clock = m_clock;
  assign _r2_do = do;
  assign _r5_p_reset = p_reset;
  assign _r5_m_clock = m_clock;
  assign _r5_do = do;
  assign _r4_p_reset = p_reset;
  assign _r4_m_clock = m_clock;
  assign _r4_do = do;
  assign _r7_p_reset = p_reset;
  assign _r7_m_clock = m_clock;
  assign _r7_do = do;
  assign _r1_p_reset = p_reset;
  assign _r1_m_clock = m_clock;
  assign _r1_do = do;
  /* assigns */
  assign out_gen = do ? {_r0_out, {_r1_out, {_r2_out, {_r3_out, {_r4_out, {_r5_out, {_r6_out, _r7_out}}}}}}} : 256'bx;
  assign _r2_fig = do ? _r1_out : 32'bx;
  assign _r6_fig = do ? _r5_out : 32'bx;
  assign _r3_fig = do ? _r2_out : 32'bx;
  assign _r7_fig = do ? _r6_out : 32'bx;
  assign _r0_fig = do ? fig : 32'bx;
  assign _r4_fig = do ? _r3_out : 32'bx;
  assign out = do ? _r7_out : 32'bx;
  assign _r1_fig = do ? _r0_out : 32'bx;
  assign _r5_fig = do ? _r4_out : 32'bx;
endmodule
/* End of file (init.v) */
