/* declare fitness4 */

module fitness32(p_reset, m_clock, init, do0, do1, in, in_val, in_vol, val, vol);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input init;
  wire init;
  input do0;
  wire do0;
  input do1;
  wire do1;
  input [31:0] in;
  wire [31:0] in;
  input [255:0] in_val;
  wire [255:0] in_val;
  input [255:0] in_vol;
  wire [255:0] in_vol;
  output [12:0] val;
  wire [12:0] val;
  output [12:0] vol;
  wire [12:0] vol;
  /* elements */
  wire [10:0] sval0;
  wire [10:0] sval1;
  wire [10:0] sval2;
  wire [10:0] sval3;
  wire [10:0] svol0;
  wire [10:0] svol1;
  wire [10:0] svol2;
  wire [10:0] svol3;
  reg [11:0] val0;
  reg [11:0] val1;
  reg [11:0] vol0;
  reg [11:0] vol1;
  /* sub-modules */
  wire [3:0] _f0_in;
  wire [31:0] _f0_in_val;
  wire [31:0] _f0_in_vol;
  wire [9:0] _f0_val;
  wire [9:0] _f0_vol;
  wire _f0_init;
  wire _f0_do;
  wire _f0_p_reset;
  wire _f0_m_clock;
  wire [3:0] _f1_in;
  wire [31:0] _f1_in_val;
  wire [31:0] _f1_in_vol;
  wire [9:0] _f1_val;
  wire [9:0] _f1_vol;
  wire _f1_init;
  wire _f1_do;
  wire _f1_p_reset;
  wire _f1_m_clock;
  wire [3:0] _f2_in;
  wire [31:0] _f2_in_val;
  wire [31:0] _f2_in_vol;
  wire [9:0] _f2_val;
  wire [9:0] _f2_vol;
  wire _f2_init;
  wire _f2_do;
  wire _f2_p_reset;
  wire _f2_m_clock;
  wire [3:0] _f3_in;
  wire [31:0] _f3_in_val;
  wire [31:0] _f3_in_vol;
  wire [9:0] _f3_val;
  wire [9:0] _f3_vol;
  wire _f3_init;
  wire _f3_do;
  wire _f3_p_reset;
  wire _f3_m_clock;
  wire [3:0] _f4_in;
  wire [31:0] _f4_in_val;
  wire [31:0] _f4_in_vol;
  wire [9:0] _f4_val;
  wire [9:0] _f4_vol;
  wire _f4_init;
  wire _f4_do;
  wire _f4_p_reset;
  wire _f4_m_clock;
  wire [3:0] _f5_in;
  wire [31:0] _f5_in_val;
  wire [31:0] _f5_in_vol;
  wire [9:0] _f5_val;
  wire [9:0] _f5_vol;
  wire _f5_init;
  wire _f5_do;
  wire _f5_p_reset;
  wire _f5_m_clock;
  wire [3:0] _f6_in;
  wire [31:0] _f6_in_val;
  wire [31:0] _f6_in_vol;
  wire [9:0] _f6_val;
  wire [9:0] _f6_vol;
  wire _f6_init;
  wire _f6_do;
  wire _f6_p_reset;
  wire _f6_m_clock;
  wire [3:0] _f7_in;
  wire [31:0] _f7_in_val;
  wire [31:0] _f7_in_vol;
  wire [9:0] _f7_val;
  wire [9:0] _f7_vol;
  wire _f7_init;
  wire _f7_do;
  wire _f7_p_reset;
  wire _f7_m_clock;
  fitness4 f0(.p_reset(_f0_p_reset),
              .m_clock(_f0_m_clock),
              .in(_f0_in),
              .in_val(_f0_in_val),
              .in_vol(_f0_in_vol),
              .val(_f0_val),
              .vol(_f0_vol),
              .init(_f0_init),
              .do(_f0_do));
  fitness4 f1(.p_reset(_f1_p_reset),
              .m_clock(_f1_m_clock),
              .in(_f1_in),
              .in_val(_f1_in_val),
              .in_vol(_f1_in_vol),
              .val(_f1_val),
              .vol(_f1_vol),
              .init(_f1_init),
              .do(_f1_do));
  fitness4 f2(.p_reset(_f2_p_reset),
              .m_clock(_f2_m_clock),
              .in(_f2_in),
              .in_val(_f2_in_val),
              .in_vol(_f2_in_vol),
              .val(_f2_val),
              .vol(_f2_vol),
              .init(_f2_init),
              .do(_f2_do));
  fitness4 f3(.p_reset(_f3_p_reset),
              .m_clock(_f3_m_clock),
              .in(_f3_in),
              .in_val(_f3_in_val),
              .in_vol(_f3_in_vol),
              .val(_f3_val),
              .vol(_f3_vol),
              .init(_f3_init),
              .do(_f3_do));
  fitness4 f4(.p_reset(_f4_p_reset),
              .m_clock(_f4_m_clock),
              .in(_f4_in),
              .in_val(_f4_in_val),
              .in_vol(_f4_in_vol),
              .val(_f4_val),
              .vol(_f4_vol),
              .init(_f4_init),
              .do(_f4_do));
  fitness4 f5(.p_reset(_f5_p_reset),
              .m_clock(_f5_m_clock),
              .in(_f5_in),
              .in_val(_f5_in_val),
              .in_vol(_f5_in_vol),
              .val(_f5_val),
              .vol(_f5_vol),
              .init(_f5_init),
              .do(_f5_do));
  fitness4 f6(.p_reset(_f6_p_reset),
              .m_clock(_f6_m_clock),
              .in(_f6_in),
              .in_val(_f6_in_val),
              .in_vol(_f6_in_vol),
              .val(_f6_val),
              .vol(_f6_vol),
              .init(_f6_init),
              .do(_f6_do));
  fitness4 f7(.p_reset(_f7_p_reset),
              .m_clock(_f7_m_clock),
              .in(_f7_in),
              .in_val(_f7_in_val),
              .in_vol(_f7_in_vol),
              .val(_f7_val),
              .vol(_f7_vol),
              .init(_f7_init),
              .do(_f7_do));
  /* invokes */
  assign _f7_p_reset = p_reset;
  assign _f7_m_clock = m_clock;
  assign _f7_init = init;
  assign _f7_do = do0;
  assign _f1_p_reset = p_reset;
  assign _f1_m_clock = m_clock;
  assign _f1_init = init;
  assign _f1_do = do0;
  assign _f4_p_reset = p_reset;
  assign _f4_m_clock = m_clock;
  assign _f4_init = init;
  assign _f4_do = do0;
  assign _f6_p_reset = p_reset;
  assign _f6_m_clock = m_clock;
  assign _f6_init = init;
  assign _f6_do = do0;
  assign _f0_p_reset = p_reset;
  assign _f0_m_clock = m_clock;
  assign _f0_init = init;
  assign _f0_do = do0;
  assign _f3_p_reset = p_reset;
  assign _f3_m_clock = m_clock;
  assign _f3_init = init;
  assign _f3_do = do0;
  assign _f2_p_reset = p_reset;
  assign _f2_m_clock = m_clock;
  assign _f2_init = init;
  assign _f2_do = do0;
  assign _f5_p_reset = p_reset;
  assign _f5_m_clock = m_clock;
  assign _f5_init = init;
  assign _f5_do = do0;
  /* assigns */
  assign _f5_in_val = init ? in_val[191:160] : 32'bx;
  assign _f0_in_vol = init ? in_vol[31:0] : 32'bx;
  assign _f4_in_vol = init ? in_vol[159:128] : 32'bx;
  assign _f1_in = do0 ? in[7:4] : 4'bx;
  assign _f0_in_val = init ? in_val[31:0] : 32'bx;
  assign svol0 = do0 ? 11'b00000000000 + {1'b0, _f0_vol + _f1_vol} : 11'bx;
  assign vol = do1 ? 13'b0000000000000 + {1'b0, vol0 + vol1} : 13'bx;
  assign svol3 = do0 ? 11'b00000000000 + {1'b0, _f6_vol + _f7_vol} : 11'bx;
  assign _f4_in_val = init ? in_val[159:128] : 32'bx;
  assign sval2 = do0 ? 11'b00000000000 + {1'b0, _f4_val + _f5_val} : 11'bx;
  assign val = do1 ? 13'b0000000000000 + {1'b0, val0 + val1} : 13'bx;
  assign _f6_in = do0 ? in[27:24] : 4'bx;
  assign svol2 = do0 ? 11'b00000000000 + {1'b0, _f4_vol + _f5_vol} : 11'bx;
  assign _f4_in = do0 ? in[19:16] : 4'bx;
  assign _f3_in_vol = init ? in_vol[127:96] : 32'bx;
  assign _f7_in_vol = init ? in_vol[255:224] : 32'bx;
  assign _f3_in_val = init ? in_val[127:96] : 32'bx;
  assign _f2_in = do0 ? in[11:8] : 4'bx;
  assign _f0_in = do0 ? in[3:0] : 4'bx;
  assign _f2_in_vol = init ? in_vol[95:64] : 32'bx;
  assign _f7_in_val = init ? in_val[255:224] : 32'bx;
  assign sval1 = do0 ? 11'b00000000000 + {1'b0, _f2_val + _f3_val} : 11'bx;
  assign _f6_in_vol = init ? in_vol[223:192] : 32'bx;
  assign svol1 = do0 ? 11'b00000000000 + {1'b0, _f2_vol + _f3_vol} : 11'bx;
  assign _f2_in_val = init ? in_val[95:64] : 32'bx;
  assign sval0 = do0 ? 11'b00000000000 + {1'b0, _f0_val + _f1_val} : 11'bx;
  assign _f6_in_val = init ? in_val[223:192] : 32'bx;
  assign _f1_in_vol = init ? in_vol[63:32] : 32'bx;
  assign _f5_in_vol = init ? in_vol[191:160] : 32'bx;
  assign _f3_in = do0 ? in[15:12] : 4'bx;
  assign _f7_in = do0 ? in[31:28] : 4'bx;
  assign sval3 = do0 ? 11'b00000000000 + {1'b0, _f6_val + _f7_val} : 11'bx;
  assign _f5_in = do0 ? in[23:20] : 4'bx;
  assign _f1_in_val = init ? in_val[63:32] : 32'bx;
  /* regUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val1 <= 12'b0;
      else if (do0)
        val1 <= 12'b000000000000 + {1'b0, sval2 + sval3};
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        vol1 <= 12'b0;
      else if (do0)
        vol1 <= 12'b000000000000 + {1'b0, svol2 + svol3};
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        val0 <= 12'b0;
      else if (do0)
        val0 <= 12'b000000000000 + {1'b0, sval0 + sval1};
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        vol0 <= 12'b0;
      else if (do0)
        vol0 <= 12'b000000000000 + {1'b0, svol0 + svol1};
    end
endmodule
/* End of file (fitness32.v) */
