module link(p_reset, m_clock, pre, put, val, vol, out0, out1);
  /* port declarations */
  input p_reset;
  wire p_reset;
  input m_clock;
  wire m_clock;
  input pre;
  wire pre;
  input put;
  wire put;
  input [7:0] val;
  wire [7:0] val;
  input [7:0] vol;
  wire [7:0] vol;
  output [247:0] out0;
  wire [247:0] out0;
  output [247:0] out1;
  wire [247:0] out1;
  /* elements */
  reg [247:0] value;
  reg [247:0] volume;
  /* assigns */
  assign out1 = put ? volume : 248'bx;
  assign out0 = put ? value : 248'bx;
  /* regUpdates */
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        value <= 248'b0;
      else if (pre)
        value <= {val, value[247:8]};
    end
  always @(posedge m_clock or posedge p_reset)
    begin
      if (p_reset)
        volume <= 248'b0;
      else if (pre)
        volume <= {vol, volume[247:8]};
    end
endmodule
/* End of file (link.v) */
